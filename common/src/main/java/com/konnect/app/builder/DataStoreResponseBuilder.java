package com.konnect.app.builder;

import com.konnect.app.pojo.BaseDTO;
import com.konnect.app.pojo.DataStoreResponse;

public class DataStoreResponseBuilder {

    private DataStoreResponse response;

    public DataStoreResponseBuilder() {
        response = new DataStoreResponse();
    }

    public DataStoreResponseBuilder status(int status) {
        response.setStatus(status);
        return this;
    }

    public DataStoreResponseBuilder qtime(int qTime) {
        response.setStatus(qTime);
        return this;
    }

    public DataStoreResponseBuilder doc(BaseDTO doc) {
        response.setDoc(doc);
        return this;
    }

    public DataStoreResponse build() {
        return this.response;
    }

}
