package com.konnect.app.utils;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.springframework.stereotype.Component;

@Component
public class ImageUtils {

    public byte[] resizeImage(int width, InputStream is) {
        BufferedImage image;
        try {
            image = ImageIO.read(is);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        float h = image.getHeight();
        float w = image.getWidth();

        float aspectRatio = h / w;

        int height = (int) ( width * aspectRatio );

        BufferedImage newImage = new BufferedImage(width, height, image.getType());


        Graphics2D graphics = newImage.createGraphics();
        graphics.drawImage(image, 0, 0, width, height, null);
        graphics.dispose();

        graphics.setComposite(AlphaComposite.Src);

        graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics.setRenderingHint(RenderingHints.KEY_RENDERING,
                        RenderingHints.VALUE_RENDER_QUALITY);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try {
            ImageIO.write(newImage, "jpeg", output);
        } catch (IOException e) {
            e.printStackTrace();
            //            return Response.serverError().entity(e).build();
        }
        byte[] imageData = output.toByteArray();
        return imageData;
    }

}
