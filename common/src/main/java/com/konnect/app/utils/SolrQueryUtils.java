package com.konnect.app.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class SolrQueryUtils {

    private final List<String> Conjunctions = Arrays.asList("and" , "And" , "AND" , "In" ,"in" , "Of" , "of" , "OF");

    public String getQueryForMultipleValue(List<String> values) {
        StringBuilder query = new StringBuilder();

        query.append("(");

        for (int i = 0; i < values.size(); i++) {
            query.append(values.get(i));

            if (i != values.size() -1) {
                query.append(" OR ");
            }
        }

        query.append(")");

        return query.toString();
    }

    public static String quoteValue(String value) {
        if (StringUtils.isBlank(value)) {
            return "\"\"";
        }

        return "\"" + value + "\"";
    }

    public String getQueryForList(List<String> values) {
        StringBuilder query = new StringBuilder();
        if(CollectionUtils.isEmpty(values))
            return "";
        values.forEach((value)->{
            query.append(quoteValue(value)).append(",");
        });
        return StringUtils.stripEnd(query.toString(), ",");
    }

    public String getQueryForList(String term,List<String> values) {
        if(CollectionUtils.isEmpty(values))
            return "";
        StringBuilder query = new StringBuilder();
        query.append(term).append(":");
        query.append(getQueryForList(values));
        return query.toString();
    }

    public String getExcludedList(String term,List<String> values){
        StringBuilder queryBuf = new StringBuilder();
        if(CollectionUtils.isEmpty(values))
            return "";
        queryBuf.append("(");
        values.forEach((id) -> {
            queryBuf.append(id).append(" OR ");
        });
        String termQuery = StringUtils.stripEnd(queryBuf.toString(), " OR ");
        return "-"+term+":" + termQuery + ")";
    }

    public Object getWildcardQuery(String term,List<String> values) {
        if(CollectionUtils.isEmpty(values))
            return "";
        StringBuilder query = new StringBuilder();
        query.append(term).append(":");
        query.append("(");
        values.forEach((value)->{
            List<String> iks = getIndividualKeywords(value);
            iks.forEach((keyword)->{
                query.append("*").append(keyword).append("*").append(" OR ");
            });

        });
        String temp = StringUtils.stripEnd(query.toString(), " OR ");
        return temp+")";
    }

    private List<String> getIndividualKeywords(String value) {
        String[] split = value.split(" ");
        List<String> keys = new ArrayList<>();
        for(String key : split){
            if(validKey(key)){
                keys.add(key);
            }
        }
        return keys;
    }

    private boolean validKey(String key) {
        if(!conjunction(key))
            return true;
        return false;
    }

    private boolean conjunction(String key) {
        return Conjunctions.contains(key);
    }

}
