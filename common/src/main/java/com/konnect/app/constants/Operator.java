package com.konnect.app.constants;

public enum Operator {
	EQ,NEQ,GTE,LTE,WILDCARD,ALGO,IN,NOTIN
}
