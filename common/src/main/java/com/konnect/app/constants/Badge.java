package com.konnect.app.constants;

import java.util.HashMap;
import java.util.Map;

public class Badge {
	private String badgeTitle;
	private Map<String, String> badgeMap = new HashMap<String, String>(){{
		put("Entrepreneurship", "entrepreneurship");
		put("Vertical Growth", "careerCounselor");
    	put("Job Change", "careerCounselor");
    	put("Skills", "coach");
    	put("Work-Life Balance", "lifeCoach");
    	put("Other", "trainer");
	}};
	
	public Badge(String category) {
		this.badgeTitle = badgeMap.get(category);
	}
	
	public String getBadgeTitle() {
		return badgeTitle;
	}
}
