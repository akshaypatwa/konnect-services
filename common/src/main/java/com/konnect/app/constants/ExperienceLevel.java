package com.konnect.app.constants;

public enum ExperienceLevel {
	EntryLevel (100,"Entry Level"), MidSenior (200,"Mid-Senior"), Senior (300,"Senior"), Leader (400,"Leader (Director & above)"), Executive (500,"Executive (VP & above)");
	
	int expLevelCode;
	String name;
	ExperienceLevel(int code,String lname){
		expLevelCode = code;
		name=lname;
	}
	@Override
	public String toString() {
		return name;
	}
	public int code(){
		return expLevelCode;
	}
	
}
