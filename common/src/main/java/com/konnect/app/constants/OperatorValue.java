package com.konnect.app.constants;

import java.util.List;

public class OperatorValue {
	
	private Operator operator;
	private List<String> values;
	
	public OperatorValue(Operator operator, List<String> values) {
		super();
		this.operator = operator;
		this.values = values;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}
	
	
	
}
