package com.konnect.app.constants;

public enum NetworkStatus {
	Approved, Pending, Declined, Deleted
}
