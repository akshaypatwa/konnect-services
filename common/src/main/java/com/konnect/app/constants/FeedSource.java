package com.konnect.app.constants;

public enum FeedSource {
	GOOGLE_ALERTS,FEEDLY,COURSERA("RSS"),WEBHOSE,AMAZON("RSS"),YOUTUBE("RSS"),KONNECT;
	String type;
	FeedSource(){};
	FeedSource(String type){
		this.type=type;
	}
	
}
