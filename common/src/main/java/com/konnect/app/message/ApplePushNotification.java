package com.konnect.app.message;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.net.ssl.SSLException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.PushNotificationResponse;
import com.turo.pushy.apns.auth.ApnsSigningKey;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;
import com.turo.pushy.apns.util.concurrent.PushNotificationFuture;
import com.turo.pushy.apns.util.concurrent.PushNotificationResponseListener;

@Component
public class ApplePushNotification {

    private static final String TEAM_ID = "DR5375W2JE";

    private static final String KEY_ID = "W6W7766KSH";

    private static final String TOPIC = "org.reactjs.native.VAXA";

    private static final String FILE_NAME = "APN_AuthKey.p8";

    private ApnsClient apnsClient;

    private ApnsClient apnsDevClient;

    @PostConstruct
    public void setup() {
        buildClient();
    }

    @PreDestroy
    public void cleanUp() {
        apnsClient.close();
    }

    private void buildDevClient() {
        ClassLoader classLoader = getClass().getClassLoader();

        InputStream stream = classLoader.getResourceAsStream(FILE_NAME);


        try {
            apnsDevClient = new ApnsClientBuilder()
                            .setApnsServer(ApnsClientBuilder.DEVELOPMENT_APNS_HOST)
                            .setSigningKey(ApnsSigningKey.loadFromInputStream(stream,
                                            TEAM_ID, KEY_ID))
                            .build();
        } catch (InvalidKeyException | NoSuchAlgorithmException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void buildClient() {
        ClassLoader classLoader = getClass().getClassLoader();

        InputStream stream = classLoader.getResourceAsStream(FILE_NAME);


        try {
            apnsClient = new ApnsClientBuilder()
                            .setApnsServer(ApnsClientBuilder.PRODUCTION_APNS_HOST)
                            .setSigningKey(ApnsSigningKey.loadFromInputStream(stream,
                                            TEAM_ID, KEY_ID))
                            .build();
        } catch (InvalidKeyException | NoSuchAlgorithmException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * Fire and forget async call to APNS.
     *
     * Let's not block other API calls on this.
     *
     * @param destination
     * @param message
     * @param fromName
     */
    public void sendAsync(String destination, String message, String title, String fromId, Navigate navigate) {

        if (StringUtils.isBlank(destination)) {
            return;
        }

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(() -> {
            try {
                send(destination, message, title, fromId, navigate);
            } catch (InvalidKeyException | NoSuchAlgorithmException | IOException | InterruptedException
                            | ExecutionException e) {
                e.printStackTrace();
            }
        });

    }

    public void send(String destination, String message, String title, String fromId, Navigate navigate) throws InvalidKeyException, SSLException, NoSuchAlgorithmException, IOException, InterruptedException, ExecutionException {
        // Use Prod APNS by default
        send(destination, message, title, fromId, navigate, false);
    }

    public void send(String destination, String message, String title, String fromId, Navigate navigate, boolean useDevClient) throws InvalidKeyException, SSLException, NoSuchAlgorithmException, IOException, InterruptedException, ExecutionException {

        if (StringUtils.isBlank(destination)) {
            System.out.println("Destination is empty");
            return;
        }

        if (apnsClient == null) {
            buildClient();
        }

        String token = TokenUtil.sanitizeTokenString(destination);

        ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
        payloadBuilder.setAlertBody(message);
        payloadBuilder.setAlertTitle(title);
        payloadBuilder.setSoundFileName(ApnsPayloadBuilder.DEFAULT_SOUND_FILENAME);
        payloadBuilder.addCustomProperty("NAVIGATE", navigate.name());
        payloadBuilder.addCustomProperty("fromId", fromId);

        String payload = payloadBuilder.buildWithDefaultMaximumLength();

        System.out.println(payload);

        SimpleApnsPushNotification notification = new SimpleApnsPushNotification(token, TOPIC, payload);
        PushNotificationFuture<SimpleApnsPushNotification, PushNotificationResponse<SimpleApnsPushNotification>> future = apnsClient.sendNotification(notification);

        future.addListener(new PushNotificationResponseListener<SimpleApnsPushNotification>() {

            @Override
            public void operationComplete(final PushNotificationFuture<SimpleApnsPushNotification, PushNotificationResponse<SimpleApnsPushNotification>> future) throws Exception {
                // When using a listener, callers should check for a failure to send a
                // notification by checking whether the future itself was successful
                // since an exception will not be thrown.
                if (future.isSuccess()) {
                    final PushNotificationResponse<SimpleApnsPushNotification> pushNotificationResponse =
                                    future.getNow();
                    if (pushNotificationResponse.isAccepted()) {
                        System.out.println("Push notification accepted by APNs gateway.");
                    } else {
                        System.out.println("Notification rejected by the APNs gateway: " +
                                        pushNotificationResponse.getRejectionReason());

                        if (pushNotificationResponse.getTokenInvalidationTimestamp() != null) {
                            System.out.println("\t…and the token is invalid as of " +
                                            pushNotificationResponse.getTokenInvalidationTimestamp());
                        }
                    }

                    // Handle the push notification response as before from here.
                } else {
                    // Something went wrong when trying to send the notification to the
                    // APNs gateway. We can find the exception that caused the failure
                    // by getting future.cause().
                    future.cause().printStackTrace();
                }

            }
        });

    }

    public static void main(String args[]) {
        try {
            new ApplePushNotification().sendAsync("1a4dc8496d1e268508c6dece66970a945cd52fa809c67f928395680f616803c0", "test", "Kasturi", "BXsZNwCHNC", Navigate.Network);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return;
    }

    public static enum Navigate {
        Message, Network, Home, Goal
    }

}
