package com.konnect.app.message;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Type;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jxmpp.jid.BareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Localpart;
import org.jxmpp.stringprep.XmppStringprepException;

public class EjabberdProxy implements MessageListener {

    private static final String ENDPOINT = "ec2-54-200-51-71.us-west-2.compute.amazonaws.com";

    private AbstractXMPPConnection mConnection;
    private String host;
    private int port;
    private String user;
    private String password;


    public EjabberdProxy(String host, int port, String user, String password) {
        this.host = host;
        this.port = port;
        this.user = user.toLowerCase();
        this.password = password;
    }


    public EjabberdProxy() {

    }

    public void statusChange(String profileId, String status)
                    throws XMPPException, XmppStringprepException, NotConnectedException, InterruptedException {

        login();

        if (mConnection != null) {
            BareJid user1 = JidCreate.bareFrom(profileId + "@" + host);

            Type type = Type.unavailable;

            if (StringUtils.equals(status, "online")) {
                type = Type.available;
            }

            Presence presence = new Presence(user1, type);
            mConnection.sendStanza(presence);
            mConnection.disconnect();
        }

    }

    public void register(String hostName, String userName, String host) {
        try {
            XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();

            config.setXmppDomain(host);
            config.setUsernameAndPassword(user, password);
            config.setHost(ENDPOINT);
            config.setPort(port);
            config.setDebuggerEnabled(true);

            config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

            mConnection = new XMPPTCPConnection(config.build());

            mConnection.connect();

            AccountManager acctMgr = AccountManager.getInstance(mConnection);

            Localpart username = Localpart.from(userName);
            acctMgr.sensitiveOperationOverInsecureConnection(true);
            System.out.println("Supports account creation " + acctMgr.supportsAccountCreation());
            acctMgr.createAccount(username, "chatPassword");
            System.out.println("Account registered successfully!");
            // }
        } catch (SmackException | IOException | InterruptedException | XMPPException e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }
    }

    public void login() throws XMPPException {
        try {
            XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();

            config.setXmppDomain(host);
            config.setUsernameAndPassword(user, password);
            config.setHost(ENDPOINT);
            config.setPort(port);
            config.setDebuggerEnabled(true);

            config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

            mConnection = new XMPPTCPConnection(config.build());

            mConnection.connect();
            if (mConnection.isConnected()) {
                System.out.println("Connected --");
            }
            mConnection.login();
            if (mConnection.isAuthenticated()) {
                System.out.println("Logged in --");
            }
        } catch (SmackException | IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void sendMessage(String msg, String to) {
        try {
            Message message = new Message(to, msg);
            // TODO: need to change this approach. login() creates mConnection
            // which conflicts seperation of concern.
            login();
            mConnection.sendStanza(message);
        } catch (SmackException | InterruptedException | XMPPException | IOException e) {

            e.printStackTrace();
        } finally {
            disconnect();
        }

    }

    public void disconnect() {
        if (mConnection != null) {
            mConnection.disconnect();
        }
    }

    @Override
    public void processMessage(Message message) {
        // TODO Auto-generated method stub
        if (message.getType() == Message.Type.chat)
            System.out.println(message.getBody());

    }

    public static void main(String args[]) throws XMPPException, IOException {

        EjabberdProxy ejPrx = new EjabberdProxy("localhost", 5222, "kazz", "K0nn3ct!");
        ejPrx.register("ec2-54-200-51-71.us-west-2.compute.amazonaws.com", "test", "ec2-54-200-51-71.us-west-2.compute.amazonaws.com");
        // declare variables
        // EjabberdProxy c = new
        // EjabberdProxy("ec2-34-212-130-91.us-west-2.compute.amazonaws.com",
        // 5222, "mentor", "chatPassword");
        // c.login();
        // send(c);
        // c.register("ec2-34-212-130-91.us-west-2.compute.amazonaws.com",
        // UUID.randomUUID().toString(),
        // "ec2-34-212-130-91.us-west-2.compute.amazonaws.com");
    }

    private static void send(EjabberdProxy c) throws XMPPException {
        String msg = "{\"type\":\"MSG\",\"fromId\":\"BXsZNwCHNC\", \"fromName\": \"Deepti\"}";

        // Enter your login information here
        // c.login("admin@ec2-34-212-130-91.us-west-2.compute.amazonaws.com",
        // "admin");
        c.login();

        System.out.println("-----");

        System.out.println("Who do you want to talk to? - Type contacts full email address:");

        String profileId = "tgduwuzkom".toLowerCase();
        String talkTo = profileId + "@ec2-34-212-130-91.us-west-2.compute.amazonaws.com";

        System.out.println("-----");
        System.out.println("All messages will be sent to " + talkTo);
        System.out.println("Enter your message in the console:");
        System.out.println("-----\n");

        c.sendMessage(msg, talkTo);

        c.disconnect();
        System.exit(0);
    }

}
