package com.konnect.app.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class Message extends BaseDTO {

    private String toId;

    private String fromId;

    private String message;

    private Long timestamp;

    private boolean read=false;

    private String fromImg;

    private String toImg;

    private String fromName;

    private String toName;

    private int unreadCount;

    public boolean getRead() {
        return read;
    }

    @Field("read")
    public void setRead(boolean read) {
        this.read = read;
    }

    public String getToImg() {
        return toImg;
    }

    public String getFromName() {
        return fromName;
    }

    @Field("fromName")
    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    @Field("toName")
    public void setToName(String toName) {
        this.toName = toName;
    }

    @Field("toImg")
    public void setToImg(String toImg) {
        this.toImg = toImg;
    }

    public String getToId() {
        return toId;
    }

    @Field("toId")
    public void setToId(String toId) {
        this.toId = toId;
    }



    public String getFromId() {
        return fromId;
    }

    @Field("fromId")
    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getFromImg() {
        return fromImg;
    }

    @Field("fromImg")
    public void setFromImg(String fromImg) {
        this.fromImg = fromImg;
    }

    public String getMessage() {
        return message;
    }

    @Field("message")
    public void setMessage(String message) {
        this.message = message;
    }


    public Long getTimestamp() {
        return timestamp;
    }

    @Field("timestamp")
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

}
