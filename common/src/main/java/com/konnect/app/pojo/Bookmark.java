package com.konnect.app.pojo;

import java.util.Date;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class Bookmark extends BaseDTO {
    
    private String articleId;
    
    private String profileId;
    
    private Date timestamp;
    
    private boolean liked;
    
    private boolean saved;
    
    private boolean shared;
    
    private boolean read;
    
    private Map<String, String> sharedMetadata;

    public String getArticleId() {
        return articleId;
    }

    @Field("articleId")
    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getProfileId() {
        return profileId;
    }

    @Field("profileId")
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Field("timestamp")
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean getLiked() {
        return liked;
    }

    @Field("liked")
    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean getSaved() {
        return saved;
    }

    @Field("saved")
    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public boolean getShared() {
        return shared;
    }

    public boolean getRead() {
		return read;
	}

    @Field("read")
	public void setRead(boolean read) {
		this.read = read;
	}

	@Field("shared")
    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public Map<String, String> getSharedMetadata() {
        return sharedMetadata;
    }

    @Field("sharedMetadata*")
    public void setSharedMetadata(Map<String, String> sharedMetadata) {
        this.sharedMetadata = sharedMetadata;
    }
    
}
