package com.konnect.app.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class Article extends BaseDTO {

    private List<String> tags;
    private String publishedProfileId;
    private String publishedProfileImg;
    private String publishedProfileName;
    private String source;
    private String title;
    private String description;
    private String category;
    private String link;
    private String imageURL;
    private Long lastCrawlTime;
    private Long publishedTime;

    private boolean liked;
    private boolean saved;
    private boolean shared;
    private boolean read;
    private String imgStdResURL;

    private Integer numHelpful;

    public Integer getNumHelpful() {
        return numHelpful;
    }

    @Field("numFoundHelpful")
    public void setNumHelpful(Integer numHelpful) {
        this.numHelpful = numHelpful;
    }

    public String getImgStdResURL() {
        return imgStdResURL;
    }

    @Field("imgStdResURL")
    public void setImgStdResURL(String imgStdResURL) {
        this.imgStdResURL = imgStdResURL;
    }

    public String getPublishedProfileId() {
        return publishedProfileId;
    }
    @Field("publishedProfileId")
    public void setPublishedProfileId(String publishedProfileId) {
        this.publishedProfileId = publishedProfileId;
    }

    public Long getPublishedTime() {
        return publishedTime;
    }

    @Field("publishedTime")
    public void setPublishedTime(Long publishedTime) {
        this.publishedTime = publishedTime;
    }

    public String getPublishedProfileName() {
        return publishedProfileName;
    }
    @Field("publishedProfileName")
    public void setPublishedProfileName(String publishedProfileName) {
        this.publishedProfileName = publishedProfileName;
    }

    public Long getLastCrawlTime() {
        return lastCrawlTime;
    }

    @Field("lastCrawlTime")
    public void setLastCrawlTime(Long lastCrawlTime) {
        this.lastCrawlTime = lastCrawlTime;
    }

    public String getTitle() {
        return title;
    }

    @Field("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getTags() {
        return tags;
    }

    @Field("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getSource() {
        return source;
    }

    @Field("source")
    public void setSource(String source) {
        this.source = source;
    }

    public String getDescription() {
        return description;
    }

    @Field("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    @Field("category")
    public void setCategory(String category) {
        this.category = category;
    }

    public String getLink() {
        return link;
    }

    @Field("link")
    public void setLink(String link) {
        this.link = link;
    }

    public String getImageURL() {
        if (null != imageURL && imageURL.contains("?")) {
            return imageURL.substring(0, imageURL.indexOf('?'));
        }
        return imageURL;
    }

    @Field("imageURL")
    public void setImageURL(String thumbImg) {
        this.imageURL = thumbImg;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public boolean isShared() {
        return shared;
    }

    public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void setShared(boolean shared) {
        this.shared = shared;
    }

    public String getPublishedProfileImg() {
        return publishedProfileImg;
    }

    @Field("publishedProfileImg")
    public void setPublishedProfileImg(String publishedProfileImg) {
        this.publishedProfileImg = publishedProfileImg;
    }

}
