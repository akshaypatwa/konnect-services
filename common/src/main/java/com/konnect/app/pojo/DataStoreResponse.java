package com.konnect.app.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class DataStoreResponse {
	private String id;
	
	private int status;
	
	private int qTime;
	
	private BaseDTO doc;

	public int getqTime() {
		return qTime;
	}
	
	@Field("qTime")
	public void setqTime(int qTime) {
		this.qTime = qTime;
	}

	public String getId() {
		return id;
	}
	
	@Field("id")
	public void setId(String id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}
	@Field("status")
	public void setStatus(int status) {
		this.status = status;
	}
	
	public BaseDTO getDoc() {
        return doc;
    }
	
	public void setDoc(BaseDTO doc) {
        this.doc = doc;
    }
}
