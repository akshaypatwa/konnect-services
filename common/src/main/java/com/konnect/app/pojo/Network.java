package com.konnect.app.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "Network")
@XmlRootElement
public class Network extends BaseDTO {

    private String mentorId;

    private String menteeId;

    private String status; // Approved, Pending, Declined, Deleted

    private boolean menteeRequested;

    private String mentorImg;

    private String menteeImg;

    private String requestorName;

    private String requestorHeadline;

    private Long createdTime;

    private Long updatedTime;

    private String firstName;

    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMentorId() {
        return mentorId;
    }

    @Field("mentorId")
    public void setMentorId(String mentorId) {
        this.mentorId = mentorId;
    }

    public String getMenteeId() {
        return menteeId;
    }

    @Field("menteeId")
    public void setMenteeId(String menteeId) {
        this.menteeId = menteeId;
    }

    public String getStatus() {
        return status;
    }

    @Field("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public boolean getMenteeRequested() {
        return menteeRequested;
    }

    @Field("menteeRequested")
    public void setMenteeRequested(boolean menteeRequested) {
        this.menteeRequested = menteeRequested;
    }

    public String getMentorImg() {
        return mentorImg;
    }

    @Field("mentorImg")
    public void setMentorImg(String mentorImg) {
        this.mentorImg = mentorImg;
    }

    public String getMenteeImg() {
        return menteeImg;
    }

    @Field("menteeImg")
    public void setMenteeImg(String menteeImg) {
        this.menteeImg = menteeImg;
    }

    public String getRequestorName() {
        return requestorName;
    }

    @Field("requestorName")
    public void setRequestorName(String requestorName) {
        this.requestorName = requestorName;
    }

    public String getRequestorHeadline() {
        return requestorHeadline;
    }

    @Field("requestorHeadline")
    public void setRequestorHeadline(String requestorHeadline) {
        this.requestorHeadline = requestorHeadline;
    }

    public Long getCreatedTime() {
        return createdTime;
    }

    @Field("createdTime")
    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedTime() {
        return updatedTime;
    }

    //    @Field("updatedTime")
    public void setUpdatedTime(Long updatedTime) {
        this.updatedTime = updatedTime;
    }

}
