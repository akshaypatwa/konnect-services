package com.konnect.app.pojo;

import java.util.Date;

import org.apache.solr.client.solrj.beans.Field;

public class Feedback extends BaseDTO {

    private String comment;

    private int rating;

    private String name;

    private String screenName;

    private String profileId;

    private Date createdOn;

    public Date getCreatedOn() {
        return createdOn;
    }

    @Field("createdOn")
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getProfileId() {
        return profileId;
    }

    @Field("profileId")
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getScreenName() {
        return screenName;
    }

    @Field("screenName")
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getComment() {
        return comment;
    }

    @Field("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    @Field("rating")
    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    @Field("name")
    public void setName(String name) {
        this.name = name;
    }

}
