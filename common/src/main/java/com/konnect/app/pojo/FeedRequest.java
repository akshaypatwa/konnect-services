package com.konnect.app.pojo;

import java.util.List;

import com.konnect.app.constants.FeedSource;

public class FeedRequest {

    private FeedSource source;

    private List<String> tags;

    public FeedSource getSource() {
        return source;
    }

    public void setSource(FeedSource source) {
        this.source = source;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

}
