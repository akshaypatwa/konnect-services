package com.konnect.app.pojo.response;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.konnect.app.pojo.Goal;

@XmlRootElement
public class GoalResponse {

    private Map<String, List<Goal>> goals;

    public Map<String, List<Goal>> getGoals() {
        return goals;
    }

    public void setGoals(Map<String, List<Goal>> goals) {
        this.goals = goals;
    }

}
