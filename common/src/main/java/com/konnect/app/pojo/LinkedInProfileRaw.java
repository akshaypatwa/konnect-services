package com.konnect.app.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class LinkedInProfileRaw {

    private String uuid;
    private String id;
    private String firstName;
    private String formattedName;
    private String headline;
    private String industry;
    private String lastName;
    private String pictureUrl;
    private String summary;
    private Location location;
    private boolean isMentor;
    private boolean isMentee;
    private String emailAddress;

    private List<String> tags;

    private PictureUrls pictureUrls;

    public static class Location {

        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public String getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public List<String> getTags() {
        return tags;
    }

    @XmlElement(name = "tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Field("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Field("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    @Field("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFormattedName() {
        return formattedName;
    }

    @Field("formattedName")
    public void setFormattedName(String formattedName) {
        this.formattedName = formattedName;
    }

    public String getHeadline() {
        return headline;
    }

    @Field("headline")
    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getIndustry() {
        return industry;
    }

    @Field("industry")
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getLastName() {
        return lastName;
    }

    @Field("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    @Field("pictureUrl")
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getSummary() {
        return summary;
    }

    @Field("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isMentor() {
        return isMentor;
    }

    @XmlElement(name = "isMentor")
    public void setMentor(boolean isMentor) {
        this.isMentor = isMentor;
    }

    @XmlElement(name = "emailAddress")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public boolean isMentee() {
        return isMentee;
    }

    @XmlElement(name = "isMentee")
    public void setMentee(boolean isMentee) {
        this.isMentee = isMentee;
    }

    public PictureUrls getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(PictureUrls pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public class PictureUrls {
        private List<String> values;

        public List<String> getValues() {
            return values;
        }

        public void setValues(List<String> values) {
            this.values = values;
        }
    }

}
