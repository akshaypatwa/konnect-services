package com.konnect.app.pojo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

/**
 * @author mac
 *
 */
@XmlRootElement
public class Goal extends BaseDTO {

	private String area;

    private String subArea;

    private String level;

    private Date expiryDate;

    private String description;

    private List<String> mentorId;

    private List<String> mentorImg;

    private String status;

    private String profileId;

    private String profileFirstName;

    private String profileLastName;

    private int numFollowing;

    private String profileImg;

    private String menteeId;
    
    private String title;
    
    private boolean read;
    
    private String tasks;
    
    private int starsCount;
    
    private String feedBackText;
    
    private LinkedInProfile menteeProfile;
    
    private boolean accepted;

    private boolean rejected;
    
    private String lastUpdatedProfileId;
    
    private Long lastUpdatedTime;
    
    public String getArea() {
        return area;
    }

    @Field("area")
    public void setArea(String area) {
        this.area = area;
    }

    public String getSubArea() {
        return subArea;
    }

    @Field("subArea")
    public void setSubArea(String subArea) {
        this.subArea = subArea;
    }

    public String getLevel() {
        return level;
    }

    @Field("level")
    public void setLevel(String level) {
        this.level = level;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    @Field("expiryDate")
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getDescription() {
        return description;
    }

    @Field("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getMentorId() {
        return mentorId;
    }

    @Field("mentorIds")
    @XmlElement(name = "mentorId")
    public void setMentorId(List<String> mentorId) {
        this.mentorId = mentorId;
    }

    public String getStatus() {
        return status;
    }

    @Field("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfileId() {
        return profileId;
    }

    @Field("profileId")
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public int getNumFollowing() {
        return numFollowing;
    }

    @Field("numFollowing")
    public void setNumFollowing(int numFollowing) {
        this.numFollowing = numFollowing;
    }

    public String getProfileFirstName() {
        return profileFirstName;
    }

    @Field("profileFirstName")
    public void setProfileFirstName(String profileFirstName) {
        this.profileFirstName = profileFirstName;
    }

    public String getProfileLastName() {
        return profileLastName;
    }

    @Field("profileLastName")
    public void setProfileLastName(String profileLastName) {
        this.profileLastName = profileLastName;
    }

    public List<String> getMentorImg() {
        return mentorImg;
    }

    @Field("mentorImg")
    @XmlElement(name = "mentorImg")
    public void setMentorImg(List<String> mentorImg) {
        this.mentorImg = mentorImg;
    }
    
    public String getProfileImg() {
		return profileImg;
	}

    @Field("profileImg")
	public void setProfileImg(String profileImg) {
		this.profileImg = profileImg;
	}

	public String getMenteeId() {
		return menteeId;
	}
	
	@Field("menteeId")
	public void setMenteeId(String menteeId) {
		this.menteeId = menteeId;
	}

	public boolean getRead() {
		return read;
	}
	
	@Field("read")
	public void setRead(boolean read) {
		this.read = read;
	}

	public String getTitle() {
		return title;
	}

	@Field("title")
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTasks() {
		return tasks;
	}

	@Field("tasks")
	public void setTasks(String tasks) {
		this.tasks = tasks;
	}

	public int getStarsCount() {
		return starsCount;
	}

	@Field("starsCount")
	public void setStarsCount(int starsCount) {
		this.starsCount = starsCount;
	}

	public String getFeedBackText() {
		return feedBackText;
	}

	@Field("feedBackText")
	public void setFeedBackText(String feedBackText) {
		this.feedBackText = feedBackText;
	}
	
    public LinkedInProfile getMenteeProfile() {
        return menteeProfile;
    }

    public void setMenteeProfile(LinkedInProfile menteeProfile) {
        this.menteeProfile = menteeProfile;
    }

    public boolean getAccepted() {
        return accepted;
    }

    @Field("accepted")
    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean getRejected() {
        return rejected;
    }

    @Field("rejected")
    public void setRejected(boolean rejected) {
        this.rejected = rejected;
    }

	public String getLastUpdatedProfileId() {
		return lastUpdatedProfileId;
	}
	
	@Field("lastUpdatedProfileId")
	public void setLastUpdatedProfileId(String lastUpdatedProfileId) {
		this.lastUpdatedProfileId = lastUpdatedProfileId;
	}

	public Long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	@Field("lastUpdatedTime")
	public void setLastUpdatedTime(Long lastUpdatedtime) {
		this.lastUpdatedTime = lastUpdatedtime;
	}

}
