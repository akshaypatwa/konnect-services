package com.konnect.app.pojo.response;

public enum ErrorCodes {

    PARAMETER_MISSING("V400", "Input parameters are null or empty"),
    NETWORK_EXISTS("V401", "Network already exists");


    private String errorCode;
    private String errorDescription;

    ErrorCodes(String errorCode, String errorDescription) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public ErrorResponse getResponse() {
        ErrorResponse response = new ErrorResponse(errorCode, errorDescription);
        return response;
    }

}
