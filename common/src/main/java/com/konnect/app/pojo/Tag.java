package com.konnect.app.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class Tag extends BaseDTO{
	
	private String name;
	
	private String description;
	
	private List<String> specialization;
	
	private List<String> industry;
	
	private List<String> related;
	
	private List<String> area;

	public String getName() {
		return name;
	}

	@Field("name")
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}
	
	@Field("description")
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Field("area")
	public void setArea(List<String> area) {
        this.area = area;
    }
	
	public List<String> getArea() {
        return area;
    }

	public List<String> getIndustry() {
		return industry;
	}

	@Field("industry")
	public void setIndustry(List<String> industry) {
		this.industry = industry;
	}

	public List<String> getRelated() {
		return related;
	}

	@Field("related")
	public void setRelated(List<String> related) {
		this.related = related;
	}

	public List<String> getSpecialization() {
		return specialization;
	}

	@Field("specialization")
	public void setSpecialization(List<String> specialization) {
		this.specialization = specialization;
	}
	

}
