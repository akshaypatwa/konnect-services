package com.konnect.app.pojo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "Profile")
@XmlRootElement
public class LinkedInProfile extends BaseDTO {

    private String id;
    private String firstName;
    private String formattedName;
    private String headline;
    private String industry;
    private String lastName;
    private String pictureUrl;
    private String location;
    private String summary;
    private String currentOccupation;
    private String prevOccupation;
    private String education;
    private String experienceLevel;

    private boolean mentor;
    private boolean mentee;
    private List<String> tags;

    private String coverImageUrl;

    private String emailAddress;

    private String badges;

    private List<String> affiliations;

    private String deviceToken;

    private Date createdTime;

    private String pictureIconUrl;

    public String getPictureIconUrl() {
        return pictureIconUrl;
    }

    @Field("pictureIconUrl")
    public void setPictureIconUrl(String pictureIconUrl) {
        this.pictureIconUrl = pictureIconUrl;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    @Field("createdTime")
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    @DynamoDBHashKey(attributeName="id")
    public String getId() {
        return id;
    }

    @Override
    @Field("id")
    public void setId(String id) {
        this.id = id;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    @Field("coverImageUrl")
    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    @Field("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFormattedName() {
        return formattedName;
    }

    @Field("formattedName")
    public void setFormattedName(String formattedName) {
        this.formattedName = formattedName;
    }

    public String getHeadline() {
        return headline;
    }

    @Field("headline")
    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getIndustry() {
        return industry;
    }

    @Field("industry")
    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getLastName() {
        return lastName;
    }

    @Field("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    @Field("pictureUrl")
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getLocation() {
        return location;
    }

    @Field("location")
    public void setLocation(String location) {
        this.location = location;
    }

    public String getSummary() {
        return summary;
    }

    @Field("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    public boolean getMentor() {
        return mentor;
    }

    @Field("mentor")
    public void setMentor(boolean mentor) {
        this.mentor = mentor;
    }

    public boolean getMentee() {
        return mentee;
    }

    @Field("mentee")
    public void setMentee(boolean mentee) {
        this.mentee = mentee;
    }

    public List<String> getTags() {
        return tags;
    }

    @Field("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getCurrentOccupation() {
        return currentOccupation;
    }

    @Field("currentOccupation")
    public void setCurrentOccupation(String currenOccupation) {
        this.currentOccupation = currenOccupation;
    }

    public String getPrevOccupation() {
        return prevOccupation;
    }

    @Field("prevOccupation")
    public void setPrevOccupation(String prevOccupation) {
        this.prevOccupation = prevOccupation;
    }

    public String getEducation() {
        return education;
    }

    @Field("education")
    public void setEducation(String education) {
        this.education = education;
    }

    @Field("emailAddress")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @Field("affiliations")
    public void setAffiliations(List<String> affiliations) {
        this.affiliations = affiliations;
    }

    public List<String> getAffiliations() {
        return affiliations;
    }

    public String getExperienceLevel() {
        return experienceLevel;
    }

    @Field("experienceLevel")
    public void setExperienceLevel(String experienceLevel) {
        this.experienceLevel = experienceLevel;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    @Field("deviceToken")
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getBadges() {
        return badges;
    }

    @Field("badges")
    public void setBadges(String badges) {
        this.badges = badges;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
