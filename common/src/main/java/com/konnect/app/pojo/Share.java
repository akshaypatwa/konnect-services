package com.konnect.app.pojo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class Share extends BaseDTO {

    private String articleId;

    private String profileId;

    private String profileImage;

    private Date timestamp;

    private List<String> shareIds;

    private String sharedArticle;
    
    public String getSharedArticle() {
		return sharedArticle;
	}
    @Field("sharedArticle")
	public void setSharedArticle(String sharedArticle) {
		this.sharedArticle = sharedArticle;
	}

	public List<String> getShareIds() {
        return shareIds;
    }

    @Field("shareIds")
    @XmlElement(name = "shareIds")
    public void setShareIds(List<String> shareIds) {
        this.shareIds = shareIds;
    }

    public String getArticleId() {
        return articleId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    @Field("profileImage")
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @Field("articleId")
    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getProfileId() {
        return profileId;
    }

    @Field("profileId")
    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Field("timestamp")
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

}
