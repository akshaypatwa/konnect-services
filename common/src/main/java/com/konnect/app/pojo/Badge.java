package com.konnect.app.pojo;

import java.util.HashMap;
import java.util.Map;

public class Badge {

	private int entrepreneurship;
	private int careerCounselor;
	private int coach;
	private int lifeCoach;
	private int trainer;

	private Map<String, Integer> map = new HashMap<String, Integer>();

	public void setBadge(Map<String, Integer> map) {
		this.map = map;
		updateValue();
	}

	public Map<String, Integer> getBadge() {
		if (map.isEmpty()) {
			map.put("entrepreneurship", entrepreneurship);
			map.put("careerCounselor", careerCounselor);
			map.put("coach", coach);
			map.put("lifeCoach", lifeCoach);
			map.put("trainer", trainer);
		}
		return map;
	}

	public String getString() {
		if (map.isEmpty()) {
			return "{\"entrepreneurship\":" + entrepreneurship + ", \"careerCounselor\":"
					+ careerCounselor + ", \"coach\":" + coach + ", \"lifeCoach\":" + lifeCoach
					+ ", \"trainer\":" + trainer + "}";
		} else {
			return "{\"entrepreneurship\":" + map.get("entrepreneurship") + ", \"careerCounselor\":"
					+ map.get("careerCounselor") + ", \"coach\":" + map.get("coach") + ", \"lifeCoach\":" + map.get("lifeCoach")
					+ ", \"trainer\":" + map.get("trainer") + "}";
		}
	}

	private void updateValue() {
		setEntrepreneurship(map.get("entrepreneurship"));
		setCareerCounselor(map.get("careerCounselor"));
		setCoach(map.get("coach"));
		setLifeCoach(map.get("lifeCoach"));
		setTrainer(map.get("trainer"));
	}

	public int getEntrepreneurship() {
		return entrepreneurship;
	}

	public void setEntrepreneurship(int entrepreneurship) {
		this.entrepreneurship = entrepreneurship;
	}

	public int getCareerCounselor() {
		return careerCounselor;
	}

	public void setCareerCounselor(int careerCounselor) {
		this.careerCounselor = careerCounselor;
	}

	public int getCoach() {
		return coach;
	}

	public void setCoach(int coach) {
		this.coach = coach;
	}

	public int getLifeCoach() {
		return lifeCoach;
	}

	public void setLifeCoach(int lifeCoach) {
		this.lifeCoach = lifeCoach;
	}

	public int getTrainer() {
		return trainer;
	}

	public void setTrainer(int trainer) {
		this.trainer = trainer;
	}

}
