package com.konnect.app.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.solr.client.solrj.beans.Field;

@XmlRootElement
public class Notification extends BaseDTO {

	private String toId;

	private Long eventTime;

	private String type;

	private String payload;

	private String fromId;

	private String message;

	private String fromPictureURL;

	private boolean read;

	public String getToId() {
		return toId;
	}

	@Field("toId")
	public void setToId(String toId) {
		this.toId = toId;
	}

	public Long getEventTime() {
		return eventTime;
	}

	@Field("eventTime")
	public void setEventTime(Long eventTime) {
		this.eventTime = eventTime;
	}

	public String getType() {
		return type;
	}

	@Field("type")
	public void setType(String type) {
		this.type = type;
	}

	public String getPayload() {
		return payload;
	}

	@Field("payload")
	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getFromId() {
		return fromId;
	}

	@Field("fromId")
	public void setFromId(String fromId) {
		this.fromId = fromId;
	}

	public String getMessage() {
		return message;
	}

	@Field("message")
	public void setMessage(String message) {
		this.message = message;
	}

	public boolean getRead() {
		return read;
	}

	@Field("read")
	public void setRead(boolean read) {
		this.read = read;
	}

	public String getFromPictureURL() {
		return fromPictureURL;
	}

	@Field("fromPictureURL")
	public void setFromPictureURL(String fromPictureURL) {
		this.fromPictureURL = fromPictureURL;
	}

}
