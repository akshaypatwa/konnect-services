package com.konnect.app.pojo;

import org.apache.solr.client.solrj.beans.Field;

public class GroupResult {
	private Long count;
	private String groupText;
	public Long getCount() {
		return count;
	}
	
	@Field("count")
	public void setCount(Long count) {
		this.count = count;
	}
	public String getGroupText() {
		return groupText;
	}
	@Field("groupText")
	public void setGroupText(String groupText) {
		this.groupText = groupText;
	}
	
}
