package com.konnect.app.gateway;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.konnect.app.pojo.LinkedInProfileRaw;

@Component
public class LinkedInGateway {

    private static final String CLIENT_ID = "8675giecwpj86y";

    private static final String CLIENT_SECRET = "YyLI9PGTpjERU6Zp";

    private static final String GRANT_TYPE = "authorization_code";

    private static final String PROFILE_URL = "https://api.linkedin.com/v1/people/~:"
                    + "(id,first-name,maiden-name,last-name,"
                    + "formatted-name,phonetic-first-name,phonetic-last-name,formatted-phonetic-name,"
                    + "headline,location,industry,"
                    + "summary,specialties,positions,picture-url,"
                    + "site-standard-profile-request,api-standard-profile-request,public-profile-url,email-address,picture-urls::(original))?format=json";


    private static final String AUTH_URL = "https://www.linkedin.com/oauth/v2/accessToken";

    public String getAuthToken(String authCode, String redirectUrl) {
        CloseableHttpClient httpclient = HttpClients.createDefault();

        List<NameValuePair> pairs = new ArrayList<>();

        pairs.add(new BasicNameValuePair("client_id", CLIENT_ID));
        pairs.add(new BasicNameValuePair("code", authCode));
        pairs.add(new BasicNameValuePair("redirect_uri", redirectUrl));
        pairs.add(new BasicNameValuePair("client_secret", CLIENT_SECRET));

        pairs.add(new BasicNameValuePair("grant_type", GRANT_TYPE));

        HttpPost post = new HttpPost(AUTH_URL);

        try {
            post.setEntity(new UrlEncodedFormEntity(pairs));
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String authToken = "";

        try {
            CloseableHttpResponse response = httpclient.execute(post);

            String str = EntityUtils.toString(response.getEntity());
            System.out.println(str);

            AuthResponse authResponse = new Gson().fromJson(str, AuthResponse.class);

            authToken = authResponse.getAccessToken();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("auth completed");

        return authToken;

    }

    public LinkedInProfileRaw getProfile(String authToken) {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpGet request = new HttpGet(PROFILE_URL);

        request.setHeader("Authorization", "Bearer " + authToken);


        CloseableHttpResponse response = null;

        try {
            response = httpClient.execute(request);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String responseString = "";
        if (response != null) {
            HttpEntity entity = response.getEntity();
            try {
                responseString = EntityUtils.toString(entity);
            } catch (ParseException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println(responseString);
        }

        LinkedInProfileRaw profile = new Gson().fromJson(responseString, LinkedInProfileRaw.class);
        return profile;

    }

    private static class AuthResponse {

        @SerializedName("access_token")
        private String accessToken;

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }
    }


}
