package com.konnect.app.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.konnect.app.constants.Operator;
import com.konnect.app.constants.OperatorValue;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.LinkedInProfileRaw;
import com.konnect.app.pojo.Network;
import com.konnect.app.utils.SolrQueryUtils;

@Component
public class ProfileDAO extends SolrBaseDAO<LinkedInProfile> {

    @Autowired
    private NetworkDAO networkDAO;

    @Autowired
    private SolrQueryUtils queryUtils;

    public void insertProfile(LinkedInProfileRaw profile) {

        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrInputDocument doc = new SolrInputDocument();

        doc.addField("uuid", UUID.randomUUID().toString());
        doc.addField("id", profile.getId());
        doc.addField("firstName", profile.getFirstName());
        doc.addField("lastName", profile.getLastName());
        doc.addField("formattedName", profile.getFormattedName());
        doc.addField("headline", profile.getHeadline());
        doc.addField("industry", profile.getIndustry());
        if (profile.getPictureUrls() != null && CollectionUtils.isNotEmpty(profile.getPictureUrls().getValues())) {
            doc.addField("pictureUrl", profile.getPictureUrls().getValues().get(0));
        }
        doc.addField("summary", profile.getSummary());
        doc.addField("pictureIconUrl", profile.getPictureUrl());

        doc.addField("createdTime", new Date());

        if (profile.getLocation() != null) {
            doc.addField("location", profile.getLocation().getName());
        }

        try {
            client.add(doc);
            client.commit();
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void update(LinkedInProfile profile) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        if (profile.getTags() == null) {
            profile.setTags(new ArrayList<>());
        }

        try {
            client.addBean(profile);
            client.commit();
        } catch (IOException | SolrServerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    public LinkedInProfile getProfile(String id) throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.set("q", "id:\"" + id + "\"");

        QueryResponse response = client.query(query);

        List<LinkedInProfile> profileList = response.getBeans(LinkedInProfile.class);

        client.close();
        if (CollectionUtils.isEmpty(profileList)) {
            return null;
        }

        return profileList.get(0);
    }

    public List<LinkedInProfile> searchQuery(String queryParam) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();
        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.set("q", "_text_:*" + queryParam + "*");
        System.out.println("query String " + query.toQueryString());
        QueryResponse response;
        try {
            response = client.query(query);
            List<LinkedInProfile> profileList = response.getBeans(LinkedInProfile.class);
            if (CollectionUtils.isEmpty(profileList)) {
                return null;
            }
            return profileList;
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    public List<LinkedInProfile> searchProfiles(int limit,int offset, String mentor, String mentee, String association, String nearMe, String company, String name) {

        SolrClient client = new HttpSolrClient.Builder(getURL()).build();
        SolrQuery query = new SolrQuery();
        QueryResponse response;
        query.set("wt", "json");
        if (limit >= 0 && offset >= 0) {
            query.setRows(limit);
            query.setStart(offset);
        }
        Map<String, String> searchParams = new HashMap<>();
        List<LinkedInProfile> profiles;
        if (!mentor.equals("false")) {
            searchParams.put("mentor", mentor);
        }
        if (!mentee.equals("false")) {
            searchParams.put("mentee", mentee);
        }
        if (company != null) {
            searchParams.put("currentOccupation", company);
        }
        if (name != null && !name.equals("undefined")){
            //            query.set("q", "_text_:*" + name + "*");
            name = name.trim();
            if (name.length() > 0) {
                searchParams.put( "_text_", "*" + name + "*");
            }
        }
        if (association != null && association.length() > 0) {
            String[] assocAray = association.split(",");
            StringBuffer buffer = new StringBuffer();
            for (String str: assocAray) {
                buffer.append(str + " OR ");
            }
            String temp = StringUtils.stripEnd(buffer.toString(), " OR ");
            searchParams.put("affiliations", "( "+ temp + " )");
        }

        if (nearMe != null && nearMe.length() > 0) {
            System.out.println("nearMe "+ nearMe.length());
            String location = nearMe.substring(0, nearMe.indexOf(" "));
            searchParams.put("location", location);
        }

        StringBuffer queryBuf = new StringBuffer();
        searchParams.forEach((field, searchTerm) -> {
            if (StringUtils.isNotEmpty(searchTerm)) {
                queryBuf.append(field).append(":").append(searchTerm).append(" AND ");
            }
        });
        // String q = queryBuf.toString();
        //          TO IMPLEMENT SPECIFIC SEARCH ON NAMES ONLY
        //        String termQuery;
        //        if (name != null && !name.equals("undefined")){
        //            String nameSearch = "firstName:*"+name+"* OR "+"lastName:*"+name+"* OR "+"formattedName:*"+name+"*";
        //            queryBuf.append(nameSearch);
        //            termQuery = queryBuf.toString();
        //        }
        //        else {
        //            termQuery = StringUtils.stripEnd(queryBuf.toString(), " AND ");
        //        }
        String termQuery = StringUtils.stripEnd(queryBuf.toString(), " AND ");
        query.setQuery(termQuery);

        try {
            response = client.query(query);
            profiles = response.getBeans(LinkedInProfile.class);
            System.out.println(profiles.size());
            return profiles;
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public List<LinkedInProfile> search(String search) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        List<Network> menteesList;
        List<Network> mentorList;
        List<String> networkId = new ArrayList<>();
        networkId.add(search);
        try {
            // TO remove network people from search result
            menteesList = networkDAO.filter(-1, -1, "mentorId", quoteValue(search));
            mentorList = networkDAO.filter(-1, -1, "menteeId", quoteValue(search));

            Iterator<Network> itrMentees = menteesList.iterator();
            while (itrMentees.hasNext()) {
                Network item = itrMentees.next();
                networkId.add(item.getMenteeId());
            }

            Iterator<Network> itrMentor = mentorList.iterator();
            while (itrMentor.hasNext()) {
                Network item = itrMentor.next();
                networkId.add(item.getMentorId());
            }

            StringBuffer queryBuf = new StringBuffer();
            queryBuf.append("(");
            networkId.forEach((id) -> {
                queryBuf.append(id).append(" , ");
            });
            String termQuery = StringUtils.stripEnd(queryBuf.toString(), " , ");


            SolrQuery query = new SolrQuery();
            query.set("wt", "json");
            query.setQuery("-id:" + termQuery + ")");
            query.setRows(50);

            query.addSort("createdTime", ORDER.desc);

            QueryResponse response;
            List<LinkedInProfile> profileList = null;

            response = client.query(query);
            profileList = response.getBeans(LinkedInProfile.class);

            return profileList;
        } catch (SolrServerException | IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        return null;
    }

    public List<String> getTags(String profileId) throws SolrServerException, IOException {
        LinkedInProfile profile = getProfile(profileId);
        if (profile == null) return null;
        return profile.getTags();
    }


    @Override
    protected String getCollectionName() {
        return "profile";
    }

    @Override
    public Class<LinkedInProfile> getDataClass() {
        return LinkedInProfile.class;
    }

    public List<LinkedInProfile> getSuggestions(Map<String, OperatorValue> valuesMap) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();
        List<LinkedInProfile> profiles = Collections.EMPTY_LIST;
        try{
            SolrQuery query = new SolrQuery();
            query.set("wt", "json");
            StringBuffer queryBuffer = new StringBuffer();
            valuesMap.forEach((key,opValue)->{
                Operator op = opValue.getOperator();
                List<String> values = opValue.getValues();
                String q;
                switch(op){
                case IN:
                    if (StringUtils.isNotEmpty(q = queryUtils.getQueryForList(key, values))) {
                        queryBuffer.append(q).append(" AND ");
                    }
                    break;
                case WILDCARD:
                    if (StringUtils.isNotEmpty(q = queryUtils.getQueryForList(key, values))) {
                        queryBuffer.append(queryUtils.getWildcardQuery(key, values)).append(" AND ");
                    }
                    break;
                case NOTIN:
                    if (StringUtils.isNotEmpty(q = queryUtils.getQueryForList(key, values))) {
                        queryBuffer.append(queryUtils.getExcludedList(key, values)).append(" AND ");
                    }
                    break;
                case ALGO:
                    if (CollectionUtils.isNotEmpty(values) && StringUtils.isNotEmpty(values.get(0))) {
                        queryBuffer.append(values.get(0)).append(" AND ");
                    }
                    break;

                }
            });
            query.setQuery(StringUtils.stripEnd(queryBuffer.toString()," AND "));
            QueryResponse response;

            response = client.query(query);
            profiles = response.getBeans(LinkedInProfile.class);
        }
        catch(IOException | SolrServerException solre){
            solre.printStackTrace();
        }
        return profiles;
    }

}
