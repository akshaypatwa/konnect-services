package com.konnect.app.domain;

import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Notification;

@Component
public class NotificationDAO extends SolrBaseDAO<Notification> {

	@Override
	protected String getCollectionName() {
		return "notification";
	}

	@Override
	protected Class<Notification> getDataClass() {
		return Notification.class;
	}

}
