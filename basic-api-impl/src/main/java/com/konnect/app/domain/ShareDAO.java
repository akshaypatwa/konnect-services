package com.konnect.app.domain;

import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Share;
import com.konnect.app.pojo.DataStoreResponse;


@Component
public class ShareDAO extends SolrBaseDAO<Share> {

    @Override
    protected String getCollectionName() {
        return "share";
    }

    @Override
    protected Class<Share> getDataClass() {
        return Share.class;
    }

    @Override
    public DataStoreResponse insert(Share data) {
        return super.insert(data);
    }

}
