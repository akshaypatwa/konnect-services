package com.konnect.app.domain;

import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Feedback;

@Component
public class FeedbackDAO extends SolrBaseDAO<Feedback> {

    @Override
    protected String getCollectionName() {
        return "feedback";
    }

    @Override
    public Class<Feedback> getDataClass() {
        return Feedback.class;
    }

}
