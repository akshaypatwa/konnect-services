package com.konnect.app.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.konnect.app.builder.DataStoreResponseBuilder;
import com.konnect.app.pojo.BaseDTO;
import com.konnect.app.pojo.DataStoreResponse;

public abstract class BaseDynamoDBDAO<T extends BaseDTO> implements BaseDAO<T> {

    private static DynamoDBMapper mapper;

    protected abstract Class<T> getDataClass();

    @Autowired
    private Environment envProp;

    @Override
    public T get(T doc) {
        DynamoDBMapper mapper = getMapper();
        T data = mapper.load(getDataClass(), doc.getId());
        return data;
    }

    @Override
    public DataStoreResponse insert(T doc) {
        DynamoDBMapper mapper = getMapper();
        mapper.save(doc);
        return (new DataStoreResponseBuilder()).status(1).doc(doc).build();
    }

    private DynamoDBMapper getMapper() {

        if (mapper == null) {
            BasicAWSCredentials awsCreds = new BasicAWSCredentials(envProp.getProperty("aws.accessKeyId"), envProp.getProperty("aws.secretKey"));

            AmazonDynamoDB dynamo = AmazonDynamoDBClientBuilder.standard()
                            .withRegion(Regions.US_WEST_2)
                            .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                            .build();
            mapper = new DynamoDBMapper(dynamo);
        }

        return mapper;
    }

}
