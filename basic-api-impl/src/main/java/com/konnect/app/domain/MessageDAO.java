package com.konnect.app.domain;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Message;

@Component
public class MessageDAO extends SolrBaseDAO<Message> {

    private static final String TIMESTAMP = "timestamp";

    public List<Message> getMessages(String profileId, int limit, int offset)
                    throws SolrServerException, IOException {
        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.setQuery("fromId:\"" + profileId + "\" OR toId:\"" + profileId + "\"");
        if (limit > 0) {
            query.setRows(limit);
        }
        if (offset >= 0) {
            query.setStart(offset);
        }
        query.addSort(TIMESTAMP, ORDER.desc);
        return query(query);
    }

    public void getMessageSummary(String profileId, String groupByField) {

    }

    public List<Message> getConversation(String profile1, String profile2, int limit, int offset, boolean desc) throws SolrServerException, IOException {
        SolrQuery query = new SolrQuery();

        StringBuilder queryTerm = new StringBuilder();
        queryTerm.append("(fromId:").append(profile1).append(" AND toId:").append(profile2).append(")");
        queryTerm.append(" OR ");
        queryTerm.append("(toId:").append(profile1).append(" AND fromId:").append(profile2).append(")");

        query.setQuery(queryTerm.toString());
        query.setRows(limit);
        query.setStart(offset);
        query.setSort("timestamp", desc ? ORDER.desc : ORDER.asc);

        List<Message> conversation = super.query(query);
        return conversation;
    }

    @Override
    protected String getCollectionName() {
        return "message";
    }

    @Override
    protected Class<Message> getDataClass() {
        return Message.class;
    }

}
