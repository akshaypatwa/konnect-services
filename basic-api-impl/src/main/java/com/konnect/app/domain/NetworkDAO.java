package com.konnect.app.domain;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Network;

@Component
public class NetworkDAO extends SolrBaseDAO<Network> {

    @Override
    protected String getCollectionName() {
        return "network";
    }

    @Override
    protected Class<Network> getDataClass() {
        return Network.class;
    }

    public List<Network> getNetworks(String profileId) throws SolrServerException, IOException {

        String profile = ClientUtils.escapeQueryChars(profileId);

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.setQuery("mentorId:\"" + profile + "\"" + " OR menteeId:\"" + profile + "\"" );

        return query(query);
    }

    public List<Network> getActiveNetworks(String profileId) throws SolrServerException, IOException {

        String profile = ClientUtils.escapeQueryChars(profileId);

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.setQuery("(mentorId:\"" + profile + "\"" + " OR menteeId:\"" + profile + "\") AND status:Approved" );


        return query(query);
    }

}
