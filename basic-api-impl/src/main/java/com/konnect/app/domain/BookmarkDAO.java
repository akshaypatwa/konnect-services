package com.konnect.app.domain;

import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Bookmark;
import com.konnect.app.pojo.DataStoreResponse;


@Component
public class BookmarkDAO extends SolrBaseDAO<Bookmark> {

    public static final String PROFILE_ID = "profileId";

    @Override
    protected String getCollectionName() {
        return "bookmarks";
    }

    @Override
    public Class<Bookmark> getDataClass() {
        return Bookmark.class;
    }

    @Override
    public Bookmark get(String profileId, String articleId) {
        return super.get("id", profileId + "," + articleId);
    }

    @Override
    public DataStoreResponse insert(Bookmark data) {
        data.setId(data.getProfileId() + "," + data.getArticleId());
        return super.insert(data);
    }

}
