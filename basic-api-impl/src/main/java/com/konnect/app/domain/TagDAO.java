package com.konnect.app.domain;

import java.io.IOException;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Component;

import com.konnect.app.pojo.DataStoreResponse;
import com.konnect.app.pojo.Tag;

@Component
public class TagDAO extends SolrBaseDAO<Tag>{
	//private static final String URL = "http://ec2-35-165-38-189.us-west-2.compute.amazonaws.com:8983/solr/tag";

	public Tag get(String id) throws SolrServerException, IOException {
		SolrClient client = getClient();

		SolrQuery query = new SolrQuery();
		query.set("wt", "json");
		query.set("q", "name:" + id);

		QueryResponse response = client.query(query);

		List<Tag> tagList = response.getBeans(Tag.class);

		if (CollectionUtils.isEmpty(tagList)) {
			return null;
		}

		return tagList.get(0);
	}
	
	public List<Tag> getAll(int limit, int offset) throws SolrServerException, IOException {
		SolrClient client = getClient();

		SolrQuery query = new SolrQuery();
		query.set("wt", "json");
		query.setQuery("*:*");
		query.setRows(limit);
		query.setStart(offset);

		QueryResponse response = client.query(query);

		List<Tag> tagList = response.getBeans(Tag.class);
		return tagList;
	}
	
	public void delete(String id) {
		SolrClient client = getClient();

		try {
			client.deleteById(id);

			client.commit();
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected String getCollectionName() {
		return "tag";
	}

	@Override
	protected Class<Tag> getDataClass() {
		return Tag.class;
	}
}
