package com.konnect.app.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.Group;
import org.apache.solr.client.solrj.response.GroupCommand;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.konnect.app.builder.DataStoreResponseBuilder;
import com.konnect.app.pojo.BaseDTO;
import com.konnect.app.pojo.DataStoreResponse;
import com.konnect.app.pojo.GroupResult;
import com.konnect.app.utils.SolrQueryUtils;

@Component
public abstract class SolrBaseDAO<T extends BaseDTO> implements BaseDAO<T> {
    //private static final String URL = "http://ec2-35-165-38-189.us-west-2.compute.amazonaws.com:8983/solr/";
    //private static final String URL = "http://localhost:8983/solr/";

    @Autowired
    private Environment envProp;

    protected abstract String getCollectionName();

    protected abstract Class<T> getDataClass();

    protected String getURL() {
        return envProp.getProperty("konnect.solr.endpoint") + getCollectionName();
    }

    protected  SolrClient getClient(){
        return new HttpSolrClient.Builder(getURL()).build();
    }

    protected DataStoreResponseBuilder getResponseBuilder(){
        return new DataStoreResponseBuilder();
    }


    @Override
    public DataStoreResponse insert(T data) {
        String id = "";
        if (data.getId() == null) {
            id = UUID.randomUUID().toString();
            data.setId(id);
        }

        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        try {
            client.addBean(data);

            UpdateResponse resp = client.commit();
            DataStoreResponse response = getResponseBuilder().status(resp.getStatus())
                            .qtime(resp.getQTime())
                            .doc(data)
                            .build();

            return response;
        } catch (IOException | SolrServerException e) {
            e.printStackTrace();
            DataStoreResponse response = getResponseBuilder().status(-1).build();
            return response;
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("There was a problem closing the Solr Client");
            }
        }
        // responseMap.put("Response", response.getResponse());

    }

    @Override
    public T get(T doc) {
        // unimplemented
        return null;
    }

    public List<T> query(SolrQuery query) throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        QueryResponse response = client.query(query);
        List<T> responseList = response.getBeans(getDataClass());
        client.close();
        return responseList;
    }

    public T get(String field, String id) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.set("q", field + ":" + id);

        QueryResponse response;
        List<T> tagList = null;
        try {
            response = client.query(query);

            tagList = response.getBeans(getDataClass());
        } catch (SolrServerException | IOException e1) {
            e1.printStackTrace();
        }

        try {
            client.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (CollectionUtils.isEmpty(tagList)) {
            return null;
        }

        return tagList.get(0);
    }

    public List<T> getList(String field, String id) throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.set("q", field + ":" + id);

        QueryResponse response = client.query(query);

        List<T> list = response.getBeans(getDataClass());

        client.close();
        return list;
    }

    public Map<String, T> batchGet(String field, Set<String> idList) {

        ExecutorService service = Executors.newFixedThreadPool(5);

        List<Callable<Pair<String, T>>> callables = new ArrayList<>();

        for (final String id: idList) {
            Callable<Pair<String, T>> callable = () -> {
                T t = get(field, id);
                return new Pair<String, T>(id, t);
            };

            callables.add(callable);
        }

        Map<String, T> toReturn = new Hashtable<>(); // hashtable instead of hashmap to be thread-safe
        try {
            service.invokeAll(callables)
            .stream()
            .forEach(future -> {
                try {
                    Pair<String, T> value = future.get();
                    if (value != null && value.second() != null) {
                        toReturn.put(value.first(), value.second());
                    }

                } catch (InterruptedException | ExecutionException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            });
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return toReturn;
    }

    public List<T> getAll(int limit, int offset, String sort, String sortOrder)
                    throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.setQuery("*:*");
        if (limit > 0) {
            query.setRows(limit);
        }
        if (offset > 0) {

            query.setStart(offset);
        }

        if (StringUtils.isNotBlank(sort)) {
            query.addSort(sort, StringUtils.equalsIgnoreCase(sortOrder, "d") ? ORDER.desc : ORDER.asc);
        }

        QueryResponse response = client.query(query);

        List<T> tagList = response.getBeans(getDataClass());
        client.close();
        return tagList;
    }

    public List<T> filterWithFields(int limit, int offset, String field, String searchTerm, String fieldToFetch)
                    throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        if (StringUtils.isNotEmpty(searchTerm)) {
            query.setQuery(field + ":" + quoteValue(searchTerm));
        }

        if (limit >= 0 && offset >= 0) {
            query.setRows(limit);
            query.setStart(offset);
        }

        query.setFields(fieldToFetch);

        QueryResponse response = client.query(query);
        client.close();
        List<T> tagList = response.getBeans(getDataClass());
        return tagList;
    }

    public List<T> filter(int limit, int offset, String field, String searchTerm)
                    throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        if (StringUtils.isNotEmpty(searchTerm)) {

            query.setQuery(field + ":" + quoteValue(searchTerm));
        }

        if (limit >= 0 && offset >= 0) {
            query.setRows(limit);
            query.setStart(offset);
        }

        QueryResponse response = client.query(query);
        client.close();
        List<T> tagList = response.getBeans(getDataClass());
        return tagList;
    }

    public void update(T doc) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();
        try {
            client.addBean(doc);
            client.commit();
        } catch (IOException | SolrServerException e) {
            e.printStackTrace();
        } finally {

            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("There was a problem closing the Solr Client");
            }
        }
    }

    public void delete(String id) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        try {
            client.deleteById(id);

            client.commit();
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("There was a problem closing the Solr Client");
            }

        }
    }

    public List<T> filter(int limit, int offset, String field, String searchTerm, String sort, boolean sortOrderAsc)
                    throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        if (StringUtils.isNotEmpty(searchTerm)) {

            query.setQuery(field + ":" + searchTerm);
        }
        query.setRows(limit);
        query.setStart(offset);

        if (StringUtils.isNotEmpty(sort)) {
            query.addSort(sort, sortOrderAsc ? ORDER.asc : ORDER.desc);
        }

        QueryResponse response = client.query(query);

        List<T> tagList = response.getBeans(getDataClass());
        client.close();
        return tagList;
    }

    public List<T> searchByField(String fieldName, String searchTerm) throws SolrServerException, IOException {
        SolrClient client = getClient();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");

        query.setQuery(fieldName + ":*" + searchTerm + "*");

        QueryResponse response = client.query(query);
        List<T> searchList = response.getBeans(getDataClass());
        client.close();
        return searchList;
    }

    /**
     *
     * @param limit
     *            - No. of rows to limit
     * @param offset
     *            - Pagination offset
     * @param searchParams
     *            - Map of search parameters
     * @param sort
     *            - sort by property
     * @param sortOrder
     *            - sort direction ascending or descending
     * @return
     * @throws SolrServerException
     * @throws IOException
     */
    public List<T> filter(int limit, int offset, Map<String, String> searchParams, String sort, boolean sortOrderDesc)
                    throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        StringBuffer queryBuf = new StringBuffer();
        searchParams.forEach((field, searchTerm) -> {
            if (StringUtils.isNotEmpty(searchTerm)) {
                queryBuf.append(field).append(":").append(searchTerm).append(" AND ");
            }
        });
        // String q = queryBuf.toString();
        String termQuery = StringUtils.stripEnd(queryBuf.toString(), " AND ");
        query.setQuery(termQuery);
        if (limit >= 0 && offset >= 0) {
            query.setRows(limit);
            query.setStart(offset);
        }

        if (StringUtils.isNotBlank(sort)) {
            query.addSort(sort, sortOrderDesc ? ORDER.desc : ORDER.asc);
        }

        QueryResponse response = client.query(query);

        List<T> tagList = response.getBeans(getDataClass());
        client.close();
        return tagList;
    }

    public List<T> filterWithOR(int limit, int offset, Map<String, String> searchParams, String sort, boolean sortOrderDesc)
                    throws SolrServerException, IOException {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        StringBuffer queryBuf = new StringBuffer();
        searchParams.forEach((field, searchTerm) -> {
            if (StringUtils.isNotEmpty(searchTerm)) {
                queryBuf.append(field).append(":").append(searchTerm).append(" OR ");
            }
        });
        // String q = queryBuf.toString();
        String termQuery = StringUtils.stripEnd(queryBuf.toString(), " OR ");
        query.setQuery(termQuery);
        if (limit >= 0 && offset >= 0) {
            query.setRows(limit);
            query.setStart(offset);
        }

        if (StringUtils.isNotBlank(sort)) {
            query.addSort(sort, sortOrderDesc ? ORDER.desc : ORDER.asc);
        }

        QueryResponse response = client.query(query);

        List<T> tagList = response.getBeans(getDataClass());
        client.close();
        return tagList;
    }
    public Map<String, List<T>> groupBy(int limit, int offset, Map<String, String> searchParams, String sort,
                    boolean sortOrderDesc, String groupField)
                                    throws SolrServerException, IOException {

        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");

        StringBuffer queryBuf = new StringBuffer();

        searchParams.forEach((field, searchTerm) -> {
            if (StringUtils.isNotEmpty(searchTerm)) {
                queryBuf.append(field).append(":").append(searchTerm).append(" AND ");
            }
        });
        // String q = queryBuf.toString();
        String termQuery = StringUtils.stripEnd(queryBuf.toString(), " AND ");
        query.setQuery(termQuery);

        query.set("group", true);
        query.set("group.field", groupField);
        query.set("group.limit", limit);
        query.set("group.offset", offset);
        if (limit > 50) {
        	query.set("rows", limit);	
        }
        

        if (StringUtils.isNotEmpty(sort)) {
            query.setSort(sort, sortOrderDesc ? ORDER.desc : ORDER.asc);
        }

        QueryResponse response = client.query(query);

        client.close();

        Map<String, List<T>> groupResponse = new HashMap<>();

        response.getGroupResponse().getValues().forEach(v -> {
            for (Group cmd: v.getValues()) {
                List<T> results = client.getBinder().getBeans(getDataClass(), cmd.getResult());
                groupResponse.put(cmd.getGroupValue(), results);
            }
        });

        return groupResponse;
    }

    public List<GroupResult> messagesFilterAndGroup(int limit, int offset, Map<String, Object> searchParams, String groupBy,
                    String sort, String sortOrder) {
        SolrClient client = new HttpSolrClient.Builder(getURL()).build();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        StringBuffer queryBuf = new StringBuffer();
        searchParams.forEach((field, searchTerm) -> {
            queryBuf.append(field).append(":").append(searchTerm).append(" AND ");
        });
        String termQuery = StringUtils.stripEnd(queryBuf.toString(), " AND ");
        query.setQuery(termQuery);
        // query.setRows(limit);
        // query.setStart(offset);
        query.set("fl", "fromId");
        query.set("group", true);
        query.set("group.field", "fromId");
        query.set("group.format", "grouped");
        System.out.println("messagefilter: " + query.toQueryString());
        // query.addSort(sort, StringUtils.equalsIgnoreCase(sortOrder,
        // "d")?ORDER.desc:ORDER.asc);
        try {
            QueryResponse gRes = client.query(query);
            List<GroupCommand> groupCommands = gRes.getGroupResponse().getValues();
            List<GroupResult> grs = new ArrayList<>();
            for (GroupCommand gc : groupCommands) {
                List<Group> groups = gc.getValues();
                for (Group group : groups) {
                    GroupResult gr = new GroupResult();
                    System.out.println(group.getGroupValue());
                    System.out.println(group.getResult().getNumFound());
                    gr.setGroupText(group.getGroupValue());
                    gr.setCount(group.getResult().getNumFound());
                    grs.add(gr);
                }
            }
            return grs;
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("There was a problem closing the Solr Client");
            }

        }
    }

    protected String quoteValue(String value) {
        return SolrQueryUtils.quoteValue(value);
    }

}
