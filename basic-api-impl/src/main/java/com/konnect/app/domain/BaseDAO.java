package com.konnect.app.domain;

import com.konnect.app.pojo.BaseDTO;
import com.konnect.app.pojo.DataStoreResponse;

public interface BaseDAO<T extends BaseDTO> {

    public T get(T doc);

    public DataStoreResponse insert(T doc);

}
