package com.konnect.app.domain;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.cxf.common.util.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Article;
import com.konnect.app.pojo.DataStoreResponse;

@Component
public class ArticleDAO extends SolrBaseDAO<Article>{

    @Override
    protected String getCollectionName() {
        return "article";
    }

    @Override
    public Class<Article> getDataClass() {
        return Article.class;
    }

    public Article get(String id) throws SolrServerException, IOException {
        SolrClient client = getClient();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.set("q", "name:" + id);

        QueryResponse response = client.query(query);

        List<Article> articleList = response.getBeans(Article.class);

        if (CollectionUtils.isEmpty(articleList)) {
            return null;
        }

        return articleList.get(0);
    }

    public List<Article> getAll(int limit, int offset) throws SolrServerException, IOException {
        SolrClient client = getClient();

        SolrQuery query = new SolrQuery();
        query.set("wt", "json");
        query.setQuery("*:*");
        query.setRows(limit);
        query.setStart(offset);

        QueryResponse response = client.query(query);

        List<Article> articleList = response.getBeans(Article.class);
        return articleList;
    }

    public DataStoreResponse insert(List<Article> articles){
        articles.forEach((data)->{
            if (data.getId() == null) {
                String id = Base64.getEncoder().encodeToString(StringUtils.toBytesASCII(data.getLink()));
                data.setId(id);
            }

            // convert tags to lowercases (for easy searching)
            data.getTags().stream().map(String::toLowerCase);
        });

        SolrClient client = getClient();

        try {
            client.addBeans(articles);

            UpdateResponse resp = client.commit();
            DataStoreResponse response = getResponseBuilder().status(resp.getStatus()).qtime(resp.getQTime()).build();

            return response;
        } catch(IOException | SolrServerException e) {
            e.printStackTrace();
            DataStoreResponse response = getResponseBuilder().status(-1).build();
            return response;
        } finally{
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("There was a problem closing the Solr Client");
            }
        }
    }
}
