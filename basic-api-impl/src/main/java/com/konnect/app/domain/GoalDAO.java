package com.konnect.app.domain;

import org.springframework.stereotype.Component;
import com.konnect.app.pojo.Goal;


@Component
public class GoalDAO extends SolrBaseDAO<Goal> {

    @Override
    protected String getCollectionName() {
        return "goal";
    }

    @Override
    protected Class<Goal> getDataClass() {
        return Goal.class;
    }

}
