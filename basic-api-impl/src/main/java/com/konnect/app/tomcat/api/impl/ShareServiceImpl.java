package com.konnect.app.tomcat.api.impl;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.ShareDAO;
import com.konnect.app.pojo.Share;
import com.konnect.app.pojo.DataStoreResponse;
import com.konnect.app.tomcat.api.ShareService;
import com.konnect.app.tomcat.api.manager.ArticleManager;

@Service("shareService")
public class ShareServiceImpl implements ShareService {

    @Autowired
    private ShareDAO shareDAO;

    @Autowired
    private ArticleManager articleManager;

    @Override
    public Response create(Share share) {
        DataStoreResponse resp = shareDAO.insert(share);
        articleManager.sendShareArticlePushNotificationAsync(share);



        GenericEntity<DataStoreResponse> entity = new GenericEntity<DataStoreResponse>(resp) {};
        return Response.ok().entity(entity).build();
    }

}
