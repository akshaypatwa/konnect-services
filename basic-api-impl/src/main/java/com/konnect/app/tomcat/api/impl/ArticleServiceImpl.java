package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.ArticleDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.domain.ShareDAO;
import com.konnect.app.pojo.Article;
import com.konnect.app.pojo.Share;
import com.konnect.app.sync.FeedSyncFacade;
import com.konnect.app.tomcat.api.ArticleService;
import com.konnect.app.tomcat.api.manager.ArticleManager;

@Service("articleService")
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleDAO articleDao;

    @Autowired
    private ProfileDAO profileDao;

    @Autowired
    private ArticleManager articleManger;

    @Autowired
    private ShareDAO shareDao;

    @Autowired
    @Qualifier(value="feedSyncFacade")
    private FeedSyncFacade feedSyncFacade;

    @Override
    public Response getArticle(String id) {
        Article article = null;
        try {
            article = articleDao.get(id);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

        return Response.ok().entity(article).build();
    }

    @Override
    public Response getArticles(int limit, int offset, List<String> tags, String category, String profileId) {

        // ignore tags from request

        List<String> profileTags = null;

        try {
            profileTags = profileDao.getTags(profileId);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Map<String, List<Article>> articles = articleManger.articleWithProfilePreference(limit, offset, profileTags, category, profileId);

        GenericEntity<Map<String, List<Article>>> entity = new GenericEntity<Map<String, List<Article>>>(articles) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    public Response create(Article article) {
        articleDao.insert(article);
        // no need to send push notification here. Front-end sends two calls. 1)create article 2)share article. push notification logic will be in ShareServiceImpl
        //        articleManger.sendCreateArticlePushNotificationAsync(article);

        return Response.ok(article).build();
    }

    @Override
    public Response update(Article tag) {
        return null;
    }

    @Override
    public Response delete(String id, String profileId) {
        Map<String, String> searchParams = new HashMap<>();
        searchParams.put("articleId", id);
        searchParams.put("profileId", profileId);
        try {
            List<Share> list = shareDao.filter(1, 0, searchParams, "profileId", false);
            if (list.size() > 0) {
                Share sharedArticle = list.get(0);
                String sharedId = sharedArticle.getId();
                shareDao.delete(sharedId);
            }

        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        articleDao.delete(id);
        return Response.ok().build();
    }

    @Override
    public Response syncArticles() {
        feedSyncFacade.syncFeeds();
        return Response.ok("{msg: \"Feed synchronization done.\"}").build();
    }

}
