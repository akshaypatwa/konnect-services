package com.konnect.app.tomcat.api.manager;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.konnect.app.domain.NetworkDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.pojo.Network;

@Component
public class UpdateNetworkImage {

    @Autowired
    private NetworkDAO networkDAO;

    @Autowired
    private ProfileDAO profileDAO;

    public void update() {

        List<Network> networkList = null;
        try {
            networkList = networkDAO.getAll(100, 0, null, null);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (networkList == null) {
            return;
        }

        for (Network network: networkList) {
            String img = getImage(network.getMentorId());

            if (img != null) {
                network.setMentorImg(img);
            }

            img = getImage(network.getMenteeId());

            if (img != null) {
                network.setMenteeImg(img);
            }

            networkDAO.update(network);

        }

    }

    private String getImage(String id) {
        try {
            return profileDAO.getProfile(id).getPictureUrl();
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }


}
