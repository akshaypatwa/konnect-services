package com.konnect.app.tomcat.api.impl;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import com.konnect.app.message.ApplePushNotification;
import com.konnect.app.message.ApplePushNotification.Navigate;
import com.konnect.app.scripts.S3Utils;
import com.konnect.app.tomcat.api.AdminService;

@Service("adminService")
public class AdminServiceImpl implements AdminService {

    @Autowired
    private S3Utils s3Utils;

    @Autowired
    private ApplePushNotification applePush;

    @Override
    public Response migrateS3(String authString) {

        String decodeString = null;

        if (StringUtils.isNotEmpty(authString)) {
            String[] basicAuth = authString.split(" ");

            byte[] decode = Base64Utils.decodeFromString(basicAuth[1]);
            decodeString = new String(decode);
        }

        if ("kasturi:K0nn3ct".equals(decodeString)) {

            try {
                s3Utils.migrateProfileImages();
            } catch (SolrServerException | IOException e) {
                e.printStackTrace();
                return Response.serverError().entity(e).build();
            }

        } else {
            return Response.serverError().build();
        }

        return null;
    }

    @Override
    public Response sendPushNotification(String destination, String authString) {

        String decodeString = null;

        if (StringUtils.isNotEmpty(authString)) {
            String[] basicAuth = authString.split(" ");

            byte[] decode = Base64Utils.decodeFromString(basicAuth[1]);
            decodeString = new String(decode);
        }

        if ("kasturi:K0nn3ct".equals(decodeString)) {
            applePush.sendAsync(destination, "", "", "", Navigate.Message);
            return Response.ok().build();
        } else {
            return Response.notAcceptable(null).build();
        }

    }

}
