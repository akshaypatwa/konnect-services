package com.konnect.app.tomcat.api.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.konnect.app.constants.NetworkStatus;
import com.konnect.app.domain.NetworkDAO;
import com.konnect.app.domain.NotificationDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.message.ApplePushNotification;
import com.konnect.app.message.ApplePushNotification.Navigate;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.Network;
import com.konnect.app.pojo.Notification;
import com.konnect.app.pojo.response.ErrorCodes;
import com.konnect.app.utils.SolrQueryUtils;

@Component
public class NetworkManager {

    @Autowired
    private NetworkDAO networkDao;

    @Autowired
    private ProfileDAO profileDao;

    @Autowired
    private NotificationDAO notificationDAO;

    @Autowired
    private ApplePushNotification pushNotification;

    public ErrorCodes preCheck(String mentorId, String menteeId) {

        if (StringUtils.isBlank(mentorId) || StringUtils.isBlank(menteeId)) {
            return ErrorCodes.PARAMETER_MISSING;
        }

        Map<String, String> query = new HashMap<>();

        query.put("mentorId", SolrQueryUtils.quoteValue(mentorId));
        query.put("menteeId", SolrQueryUtils.quoteValue(menteeId));

        List<Network> response = null;
        try {
            response = networkDao.filter(-1, -1, query, null, false);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(response)) {
            Network network = response.get(0);
            if (!network.getStatus().equals("Approved")) {
                return ErrorCodes.NETWORK_EXISTS;
            }
        }
        return null;
    }

    public void updateImages(String profileId, String imageURL) {

        List<Network> networkList = null;
        try {
            networkList = networkDao.getNetworks(profileId);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return;
        }

        if (CollectionUtils.isEmpty(networkList)) {
            return;
        }

        for (Network network: networkList) {
            if (network.getMentorId().equals(profileId)) {
                network.setMentorImg(imageURL);
            } else {
                network.setMenteeImg(imageURL);
            }
            networkDao.update(network);
        }

    }

    private HashMap<String, String> getSearchparams(String profileId, String field) {
        Map<String, String> searchParams = new HashMap<>();
        searchParams.put(field, SolrQueryUtils.quoteValue(profileId));
        searchParams.put("status", NetworkStatus.Approved.name());
        return (HashMap<String, String>) searchParams;
    }

    public Set<String> getNetworkListOfApproved(String profileId) {
        List<Network> networksMentor = new ArrayList<>();
        List<Network> networksMentee = new ArrayList<>();
        Map<String,String> searchParamsMentor = getSearchparams(profileId, "menteeId");
        Map<String,String> searchParamsMentee = getSearchparams(profileId, "mentorId");

        try {
            networksMentor = networkDao.filter(-1, -1, searchParamsMentor, "createdTime", true);
            networksMentee = networkDao.filter(-1, -1, searchParamsMentee, "createdTime", true);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Set<String> dict = new HashSet<String>();

        networksMentor.stream().forEach(network -> {
            dict.add(network.getMentorId());
        });

        networksMentee.stream().forEach(network -> {
            dict.add(network.getMenteeId());
        });
        return dict;
    }

    public void postNotification(Network network) {
        Notification notification = new Notification();
        String status = network.getStatus();
        String message = "";
        String fromId = "";
        String fromPictureUrl = "";
        String toId = "";
        String title = "";


        if (status.equals("Pending")) {
            message = network.getRequestorName() + " has sent you a connection request";
            title = "New Connection Request!";
            fromId = network.getMenteeId();
            fromPictureUrl = network.getMenteeImg();
            toId = network.getMentorId();


        } else {
            toId = network.getMenteeId();
            fromId = network.getMentorId();
            fromPictureUrl = network.getMentorImg();
            LinkedInProfile toProfile = null;
            LinkedInProfile fromProfile = null;
            String name = "A connection";
            try {
                toProfile = profileDao.getProfile(toId);
                fromProfile = profileDao.getProfile(fromId);
            } catch (SolrServerException | IOException e) {
                e.printStackTrace();
            }

            if (toProfile != null) {
                name = toProfile.getFirstName();
            }

            if (status.equals("Decline")) {
                message = "You request has been Declined.";
                title = null;
            } else if (status.equals("Approved")) {
                message = fromProfile.getFirstName() +  " has accepted your mentorship request.";
                title = "Congrats!";
            }
        }

        notification.setFromId(fromId);
        notification.setFromPictureURL(fromPictureUrl);
        notification.setToId(toId);
        notification.setEventTime(network.getCreatedTime());
        notification.setType("Connection");
        notification.setMessage(message);
        notification.setRead(false);
        notification.setPayload("");
        notificationDAO.insert(notification);

        try {
            if (title != null) {
                LinkedInProfile toProfile = profileDao.getProfile(toId);

                if (StringUtils.isNotBlank(toProfile.getDeviceToken())) {
                    pushNotification.sendAsync(toProfile.getDeviceToken(), message, title, fromId, Navigate.Network);
                }
            }

        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


}

