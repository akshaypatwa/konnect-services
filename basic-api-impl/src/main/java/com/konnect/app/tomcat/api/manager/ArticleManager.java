package com.konnect.app.tomcat.api.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.konnect.app.domain.ArticleDAO;
import com.konnect.app.domain.BookmarkDAO;
import com.konnect.app.domain.NetworkDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.domain.ShareDAO;
import com.konnect.app.message.ApplePushNotification;
import com.konnect.app.message.ApplePushNotification.Navigate;
import com.konnect.app.pojo.Article;
import com.konnect.app.pojo.Bookmark;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.Network;
import com.konnect.app.pojo.Share;

@Component
public class ArticleManager {


    private static final Logger logger = LoggerFactory.getLogger(ArticleManager.class);

    private static final String NEW_ACTIVITY = "New Activity!";


    @Autowired
    private ArticleDAO articleDao;

    @Autowired
    private BookmarkDAO bookmarkDao;

    @Autowired
    private ApplePushNotification pushNotification;

    @Autowired
    private ShareDAO shareDAO;

    @Autowired
    private NetworkDAO networkDao;

    @Autowired
    private ProfileDAO profileDao;

    @Autowired
    private NetworkManager networkManager;

    public Map<String, List<Article>> articleWithProfilePreference(int limit, int offset, List<String> tags, String category,
                    String profileId) {
        Map<String, List<Article>> articles = null;

        List<Bookmark> bookmarks;
        List<Share> sharedArticles;
        Set<String> approvedNetworkList = networkManager.getNetworkListOfApproved(profileId);
        approvedNetworkList.add(profileId);
        String queryIds = StringUtils.join(approvedNetworkList, " OR ");
        Map<String, String> query = new HashMap<>();

        if (StringUtils.isNotEmpty(category)) {
            query.put("category", category);
        }

        //        if (CollectionUtils.isNotEmpty(tags)) {
        //            query.put("tags", queryUtils.getQueryForMultipleValue(tags));
        //        } else {
        //            query.put("tags", "*");
        //        }
        if (category != null && category.equals("Activity") && StringUtils.isNotEmpty(queryIds)) {
            query.put("publishedProfileId", "(" + profileId + ")");
        }

        query.put("tags", "*");

        try {
            articles = articleDao.groupBy(limit, offset, query, "publishedTime", true, "category");
            bookmarks = bookmarkDao.filter(100, 0, BookmarkDAO.PROFILE_ID, profileId);
            query.clear();
            query.put("shareIds", profileId);
            // commented as we don't want duplicates 
//            query.put("profileId", profileId);
            sharedArticles = shareDAO.filterWithOR(limit, offset, query, "timestamp", true);
            articles.forEach((index, itemList) -> {
                if (index.equals("Activity")) {
                    sharedArticles.forEach((article) -> {
                        if (article.getSharedArticle() != null) {
                            Gson g = new Gson();
                            Article shared = g.fromJson(article.getSharedArticle(), Article.class);
                            // update NumHelpFul count in stored shared objects.
                            Article art = new Article();
                            art = articleDao.get("id", shared.getId());
                            if ( art != null && art.getNumHelpful() != null) {
                            	shared.setNumHelpful(art.getNumHelpful());	
                            }
                            
                             
                            //							if (!shared.getSource().equals("Vaxa")) {
                            //								// Need to this Because of different implementation requirement of front-end
                            //								// should be removed after front-end changes
                            //								String title = shared.getTitle();
                            //								String description = shared.getDescription();
                            //								shared.setTitle(description);
                            //								shared.setDescription(title);
                            //							}
                            //                            if (!shared.getCategory().equals("Activity")) {
                            shared.setCategory("Activity");
                            itemList.add(shared);
                            //                            }
                        }
                    });
                }
            });
            
            // if user doesn't have own activity but has activity shared with him
            if(!(articles.size() > 0) && sharedArticles.size() > 0) {
                List<Article> sharedList = new ArrayList<Article>();
                sharedArticles.forEach((article) -> {
                    if (article.getSharedArticle() != null) {
                        Gson g = new Gson();
                        Article shared = g.fromJson(article.getSharedArticle(), Article.class);
                        shared.setCategory("Activity");
                        sharedList.add(shared);
                    }
                });
                articles.put("Activity", sharedList);
            }

            stitch(articles, bookmarks);

        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

        return articles;
    }

    public void sendShareArticlePushNotificationAsync(Share share) {

        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {

            try {
                LinkedInProfile profile = profileDao.getProfile(share.getProfileId());
                String pushMessage = profile.getFirstName() + " shared a new article.";

                if (CollectionUtils.isNotEmpty(share.getShareIds())) {

                    for (String id: share.getShareIds()) {
                        LinkedInProfile p = profileDao.getProfile(id);

                        if (StringUtils.isNotBlank(p.getDeviceToken())) {
                            pushNotification.sendAsync(p.getDeviceToken(), pushMessage, NEW_ACTIVITY, profile.getId(), Navigate.Home);
                        }
                    }

                }
            } catch (SolrServerException | IOException e) {
                // TODO Auto-generated catch block
                logger.error("Error sending push notification for share article", e);
            }


        });
    }

    public void sendCreateArticlePushNotificationAsync(Article article) {
        final String profileId = article.getPublishedProfileId();

        if (StringUtils.isBlank(profileId)) {
            logger.info("Empty profile id received");
            return;
        }

        final String pushMessage = article.getPublishedProfileName() + " posted a new article.";

        Executor executor = Executors.newSingleThreadExecutor();
        logger.info("Sending push notification for article", article.getTitle());
        executor.execute(() -> {
            try {
                sendPushNotificationsToNetwork(profileId, pushMessage);



            } catch (SolrServerException | IOException e) {
                logger.error("Exception during sending push notification", e);
            }
        });

    }

    private void sendPushNotificationsToNetwork(final String profileId, final String pushMessage)
                    throws SolrServerException, IOException {
        List<Network> networks = networkDao.getActiveNetworks(profileId);

        for (Network network: networks) {

            System.out.println("profileId: " + profileId);
            String id = profileId.equals(network.getMenteeId()) ? network.getMentorId() : network.getMenteeId();
            System.out.println("Id: " + profileId);
            List<LinkedInProfile> profiles = profileDao.filterWithFields(100, 0, "id", id, "deviceToken");

            for (LinkedInProfile profile: profiles) {
                String deviceToken = profile.getDeviceToken();
                if (StringUtils.isNotBlank(deviceToken)) {
                    pushNotification.sendAsync(deviceToken, pushMessage, NEW_ACTIVITY, "", Navigate.Home);
                }
            }
        }
    }

    private Map<String, List<Article>> stitch(Map<String, List<Article>> articles, List<Bookmark> bookmarks) {

        Map<String, Article> articleDict = new HashMap<>();

        for (List<Article> articleList: articles.values()) {
            for (Article article: articleList) {
                articleDict.put(article.getId(), article);
            }
        }

        for (Bookmark bookmark: bookmarks) {
            String articleId = bookmark.getArticleId();

            if (articleDict.containsKey(articleId)) {
                Article article = articleDict.get(articleId);

                article.setLiked(bookmark.getLiked());
                article.setSaved(bookmark.getSaved());
                article.setShared(bookmark.getShared());
                article.setRead(bookmark.getRead());
            }
        }

        return articles;
    }


}
