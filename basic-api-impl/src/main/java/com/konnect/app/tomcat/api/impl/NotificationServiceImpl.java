package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.NotificationDAO;
import com.konnect.app.pojo.Notification;
import com.konnect.app.tomcat.api.NotificationService;
import com.konnect.app.utils.SolrQueryUtils;

@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    private NotificationDAO dao;

    @Override
    public Response getData(String id) {
        Notification data = dao.get("id", id);
        return Response.ok().entity(data).build();
    }

    @Override
    public Response getDataList(int limit, int offset, String profileId) {
        List<Notification> data = null;
        String safeProfileId = SolrQueryUtils.quoteValue(profileId);
        try {
            data = dao.filter(limit, offset, "toId", safeProfileId, "eventTime", false);
        } catch (SolrServerException | IOException e) {
            return Response.serverError().entity(e).build();
        }

        GenericEntity<List<Notification>> entity = new GenericEntity<List<Notification>>(data) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    public Response create(Notification data) {
        dao.insert(data);
        return Response.ok().build();
    }

    @Override
    public Response update(Notification data) {
        dao.insert(data);
        return Response.ok().build();
    }

    @Override
    public Response delete(String id) {
        dao.delete(id);
        return Response.ok().build();
    }

    @Override
    public Response readAll(String profileId) {
        List<Notification> data ;
        Map<String,String> searchParams = new HashMap<>();
        searchParams.put("fromId", profileId);
        searchParams.put("read", "false");
        try {
            data = dao.filter(-1, -1, searchParams, "eventTime", true);
            data.forEach((item) -> {
                item.setRead(true);
                dao.insert(item);
            });
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }
        // TODO Auto-generated method stub
        return Response.ok().build();
    }
}
