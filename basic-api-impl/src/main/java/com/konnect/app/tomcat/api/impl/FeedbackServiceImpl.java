package com.konnect.app.tomcat.api.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.FeedbackDAO;
import com.konnect.app.pojo.Feedback;
import com.konnect.app.tomcat.api.FeedbackService;

@Service("feedbackService")
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackDAO feedbackDAO;

    @Override
    public Response create(Feedback feedback) {
        feedbackDAO.insert(feedback);
        return Response.ok(feedback).build();
    }

}
