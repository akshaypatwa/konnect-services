package com.konnect.app.tomcat.api.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.konnect.app.domain.MessageDAO;
import com.konnect.app.pojo.Message;

@Component
public class MessageManager {

    @Autowired
    private MessageDAO messageDAO;

    public List<Message> getSummaryList(String profileId) {

        Map<String, String> fromQuery = new HashMap<>();
        fromQuery.put("fromId", profileId);

        Map<String, String> toQuery = new HashMap<>();
        toQuery.put("toId", profileId);

        Map<String, List<Message>> fromMessages = null;
        Map<String, List<Message>> toMessages = null;
        try {
            fromMessages = messageDAO.groupBy(1, 0, fromQuery, "timestamp", true, "toId");
            toMessages = messageDAO.groupBy(1, 0, toQuery, "timestamp", true, "fromId");
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

        return stitch(fromMessages, toMessages);

    }

    private List<Message> stitch(Map<String, List<Message>> messages1, Map<String, List<Message>> messages2) {

        Map<String, Message> merge = new HashMap<>();

        Set<String> keys = new HashSet<>(messages1.keySet());

        keys.addAll(messages2.keySet());

        for (String k: keys) {
            if (k == null) {
                continue;
            }
            Message m1 = null;
            if (messages1.containsKey(k) && CollectionUtils.isNotEmpty(messages1.get(k))) {
                m1 = messages1.get(k).get(0);
            }

            Message m2 = null;
            if (messages2.containsKey(k) && CollectionUtils.isNotEmpty(messages2.get(k))) {
                m2 = messages2.get(k).get(0);
            }

            Message lastMessage;
            if (m1 == null) {
                lastMessage = m2;
            } else if (m2 == null) {
                lastMessage = m1;
            } else {
                if (m1.getTimestamp() > m2.getTimestamp()) {
                    lastMessage = m1;
                } else {
                    lastMessage = m2;
                }
            }

            merge.put(k, lastMessage);
        }

        List<Message> messages = new ArrayList<>(merge.values());
        messages.sort((m1,m2) -> m2.getTimestamp().compareTo(m1.getTimestamp()));

        return messages;
    }
}
