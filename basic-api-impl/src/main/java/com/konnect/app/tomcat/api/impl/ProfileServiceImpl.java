package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.solr.client.solrj.SolrServerException;
import org.jivesoftware.smack.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.ArticleDAO;
import com.konnect.app.domain.GoalDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.pojo.Article;
import com.konnect.app.pojo.Goal;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.request.UpdateProfileDeviceToken;
import com.konnect.app.tomcat.api.ProfileService;
import com.konnect.app.tomcat.api.manager.NetworkManager;
import com.konnect.app.tomcat.api.manager.ProfileManager;

@Service("profileService")
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    private ProfileDAO profileDao;

    @Autowired
    private NetworkManager networkManager;

    @Autowired
    private ProfileManager profileManager;

    @Autowired
    private GoalDAO goalDAO;

    @Autowired
    private ArticleDAO articleDao;
    
    @Override
    public Response getProfile(String id) {
        LinkedInProfile response = null;
        try {
            response = profileDao.getProfile(id);
        } catch (SolrServerException | IOException e) {
            Response.serverError().entity(e).build();
        }
        return Response.ok(response).build();
    }

    @Override
    public Response updateProfile(LinkedInProfile profile) {
        profileDao.update(profile);

        networkManager.updateImages(profile.getId(), profile.getPictureUrl());

        return Response.ok(profile).build();
    }

    @Override
    public Response search(String search, String query) {
        System.out.println("search " + search +" query " + query);
        if (StringUtils.isNotEmpty(query)) {
            List<LinkedInProfile> searchResults = profileDao.searchQuery(query);
            if (searchResults == null) {
                searchResults = new ArrayList<>();
            }
            GenericEntity<List<LinkedInProfile>> entity = new GenericEntity<List<LinkedInProfile>>(searchResults) {};
            return Response.ok().entity(entity).build();
        }
        List<LinkedInProfile> profiles = profileDao.search(search);
        GenericEntity<List<LinkedInProfile>> entity = new GenericEntity<List<LinkedInProfile>>(profiles) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    public Response delete(String id) {
        profileDao.delete(id);
        return Response.ok().build();
    }

    @Override
    public Response updateDeviceToken(UpdateProfileDeviceToken request) {
        profileManager.updateDeviceToken(request.getProfileId(), request.getDeviceToken());
        return Response.ok().build();
    }

	@Override
	public Response filter(boolean isMentor, boolean isMentee) {
		System.out.println("isMentor " + isMentor + " isMentee " + isMentee);
		Map<String, String> searchParam = new HashMap<String, String>();
		List<LinkedInProfile> profiles;
		if (isMentor) {
			searchParam.put("mentor", String.valueOf(isMentor));	
		}
		if (isMentee) {
			searchParam.put("mentee", String.valueOf(isMentee));	
		}
		try {
			profiles = profileDao.filter(20, 0, searchParam, "", true);
			System.out.println(profiles.size());
			GenericEntity<List<LinkedInProfile>> entity = new GenericEntity<List<LinkedInProfile>>(profiles) {};
	        return Response.ok().entity(entity).build();
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public Response SearchProfiles(int limit, int offset, String mentor, String mentee,
                                    String association, String nearMe, String company, String name)
    {        
        List<LinkedInProfile> profiles = profileDao.searchProfiles(limit,offset,mentor,mentee,association,nearMe,company,name);
        GenericEntity<List<LinkedInProfile>> entity = new GenericEntity<List<LinkedInProfile>>(profiles) {};
        return Response.ok().entity(entity).build();
    }

	@Override
	public Response getEliteProfile(String id) {
		Map<String, String> searchParams = new HashMap<>();
		LinkedHashMap<String, List<Goal>> sorted = null;
		Map<String, List<Article>> articleMap = new LinkedHashMap<>();
		Set<String> eliteIds = new LinkedHashSet<>();
		long currentTime =  new Date().getTime();
		long rangeTime = subtractDay(150);
		searchParams.put("lastUpdatedTime", "["+ rangeTime +" TO "+ currentTime + "]");
		searchParams.put("-profileId", id);
		try {
			Map<String, List<Goal>> result = goalDAO.groupBy(100, 0, searchParams, "lastUpdatedTime", true, "profileId");
			sorted = arrangeBySize(result);
			System.out.println(" sorted " + sorted.size());
			if (sorted.size() > 10) {				
				sorted = extractTop(sorted);
				eliteIds = getKeySet(sorted);
			} else if (sorted.size() < 10) {
				if (!sorted.isEmpty()) {
					eliteIds = getKeySet(sorted);
				}
				
				Set<String> articleIds = new LinkedHashSet<>();
				searchParams = new HashMap<>();
				searchParams.put("category", "Activity");
				searchParams.put("-publishedProfileId", id);
				searchParams.put("publishedTime", "["+ rangeTime +" TO "+ currentTime + "]");
				articleMap = articleDao.groupBy(100, 0, searchParams, "publishedTime", true, "publishedProfileId");
				articleMap = arrangeBySize(articleMap);
				articleIds = getKeySet(articleMap);
				if (!articleIds.isEmpty()) {
					eliteIds.addAll(articleIds);
				}
			}
			
			StringBuffer profileIds = new StringBuffer();
			String profiles = "";
			eliteIds.forEach((String s) -> {
				profileIds.append(s + " OR ");
			} );
			 if (org.apache.commons.lang3.StringUtils.isNotEmpty(profileIds)) {
				 profiles =  org.apache.commons.lang3.StringUtils.stripEnd(profileIds.toString(), " OR ");
			 }
			 profiles =  "( " + profiles + " )";
//			 System.out.println("profileIds " + profiles);
			 searchParams = new HashMap<>();
			 searchParams.put("id", profiles);
			List<LinkedInProfile> eliteProfiles = profileDao.filter(10, 0, searchParams, "id", true);
			return Response.ok(eliteProfiles).build();
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private <T> LinkedHashMap<String, List<T>> arrangeBySize(Map<String, List<T>> map) {
		LinkedHashMap<String, List<T>> resultMap;
		resultMap = map.entrySet().stream()
				.sorted(Comparator.comparingInt(e -> ((List<T>) ((Entry) e).getValue()).size()).reversed())
				// .limit(10)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> {
					throw new AssertionError();
				}, LinkedHashMap::new));
		return resultMap;
	}
	
	private <T> LinkedHashMap<String, List<T>> extractTop(LinkedHashMap<String, List<T>> data) {
		LinkedHashMap<String, List<T>> sorted = data;
		final Set<String> keySet = sorted.keySet().stream().limit(10).collect(Collectors.toSet());
		Set<String> clashingKeySet;
		int value = sorted.get((sorted.keySet().toArray())[10]).size();

		/*
		 * map id with same number of T clashing at bottom
		 */
		LinkedHashMap<String, List<T>> sortedMap = sorted.entrySet().stream().filter(e -> e.getValue().size() == value)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> {
					throw new AssertionError();
				}, LinkedHashMap::new));

		// slice top 10 from the map
		sorted = sorted.entrySet().stream().limit(10)
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> {
					throw new AssertionError();
				}, LinkedHashMap::new));

		if (!sortedMap.isEmpty()) {
			// If found clashing ID
			// remove clashing id from top 10 result
			clashingKeySet = sortedMap.keySet();
			for (String item : clashingKeySet) {
				if (keySet.contains(item)) {
					sorted.remove(item);
				}
			}

			sorted.putAll(sortedMap);
		}
		return sorted;
	}
	
	private <T> Set<String> getKeySet(Map<String, List<T>> dataMap) {
		Set<String> keySet = new HashSet<>();
		if (!dataMap.isEmpty()) {
			dataMap.entrySet().forEach(entry -> {
				keySet.add(entry.getKey());
			});
		}
		return keySet;
	}

	private static long subtractDay(int day) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DAY_OF_MONTH, -day);
		return cal.getTimeInMillis();
	}
}
