package com.konnect.app.tomcat.api.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.scripts.S3Utils;
import com.konnect.app.tomcat.api.ImageService;
import com.konnect.app.utils.ImageUtils;

@Service("imageService")
public class ImageServiceImpl implements ImageService {

    private static final String RESIZE_SUFFIX = "-resize-";

    @Autowired
    private S3Utils s3utils;

    @Autowired
    private ImageUtils imageUtils;

    @Override
    public Response getImage(UriInfo uriInfo, String fileName, int width) {

        if (StringUtils.contains(fileName, "/static/")) {
            fileName = StringUtils.substringAfterLast(fileName, "/");

            if (fileName.contains("?")) {
                fileName = StringUtils.substringBeforeLast(fileName, "?");
            }
        } else {
            URI uri = uriInfo.getRequestUri();

            String uriString = "";
            String redirectString = "";
            if (uri != null) {
                uriString = uri.toString();
            }

            if (uriString.contains("fileName=")) {
                redirectString = uriString.substring(uriString.lastIndexOf("fileName=") + 9);

                // Linkedin image url hack here --

                if (redirectString.contains("&e=")) {
                    redirectString = redirectString.substring(0, redirectString.indexOf("&e="));
                }
            }
            URI returnURI = URI.create(redirectString);
            returnURI = returnURI.normalize();
            return Response.temporaryRedirect(URI.create(redirectString)).build();
        }

        String resizedFileName = fileName + RESIZE_SUFFIX + width;
        InputStream resizedFileStream = s3utils.getResource(resizedFileName);

        if (resizedFileStream != null) {
            try {
                byte[] imageData = IOUtils.toByteArray(resizedFileStream);
                return Response.ok(new ByteArrayInputStream(imageData)).build();
            } catch (IOException e) {
                // cache-miss. continue with normal flow
                e.printStackTrace();
            }
        }


        InputStream fileStream = s3utils.getResource(fileName);
        byte[] imageData = imageUtils.resizeImage(width, fileStream);

        if (imageData == null) {
            return Response.serverError().build();
        }

        ByteArrayInputStream inputStream = new ByteArrayInputStream(imageData);

        s3utils.putResource(resizedFileName, inputStream);
        return Response.ok(imageData).type("image/png").build();
    }


}
