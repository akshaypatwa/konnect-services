package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.gateway.LinkedInGateway;
import com.konnect.app.message.EjabberdProxy;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.LinkedInProfileRaw;
import com.konnect.app.tomcat.api.LoginService;

@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LinkedInGateway gateway;

    @Autowired
    private ProfileDAO dao;

    @Override
    public Response linkAccount(String code, UriInfo ui) {
        String path = ui.getAbsolutePath().toString();
        String token = gateway.getAuthToken(code, path);
        LinkedInProfileRaw profile = gateway.getProfile(token);

        // check if profile exists already
        String profileId = profile.getId();
        LinkedInProfile profileInSolr = null;
        try {
            profileInSolr = dao.getProfile(profileId);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (profileInSolr == null) {
            EjabberdProxy ejPrx = new EjabberdProxy("localhost", 5222, "kazz", "K0nn3ct!");
            ejPrx.register("ec2-54-200-51-71.us-west-2.compute.amazonaws.com", profile.getId(), "ec2-54-200-51-71.us-west-2.compute.amazonaws.com");
            dao.insertProfile(profile);
        } else {

            // update missing fields that were added later on - e.g. emailAddress

            if (profileInSolr.getEmailAddress() == null) {
                profileInSolr.setEmailAddress(profile.getEmailAddress());
                dao.update(profileInSolr);
            }

        }
        return Response.temporaryRedirect(URI.create("/login/success?id=" + profile.getId())).build() ;
    }

    @Override
    public Response authCompleted(String id) {
        if (id != null) {
            return Response.ok().entity("Auth Success").build();
        }

        return Response.serverError().build();

    }

    @Override
    public Response statusChange(String profileId, String status) {
        EjabberdProxy ejPrx = new EjabberdProxy("ec2-54-200-51-71.us-west-2.compute.amazonaws.com", 5222, profileId, "chatPassword");

        try {
            ejPrx.statusChange(profileId, status);
        } catch (Exception e) {
            return Response.serverError().entity(e).build();
        }

        return Response.ok().build();
    }


}
