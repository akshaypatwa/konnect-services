package com.konnect.app.tomcat.api.impl;

import java.net.URI;

import javax.ws.rs.core.Response;

import org.springframework.stereotype.Service;

import com.konnect.app.tomcat.api.StaticService;

/**
 * @author Kazz.
 */
@Service( "staticService" )
public class StaticServiceImpl implements StaticService {

    public static final String S3_BUCKET = "https://s3-us-west-2.amazonaws.com/com.myvaxa.resources/";

    @Override
    public Response getResource( String resource )
    {
        return Response.temporaryRedirect(URI.create(S3_BUCKET + resource)).build();
    }
}
