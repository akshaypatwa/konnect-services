package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.jivesoftware.smack.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.konnect.app.algo.Algorithm;
import com.konnect.app.algo.Algorithms;
import com.konnect.app.constants.NetworkStatus;
import com.konnect.app.constants.Operator;
import com.konnect.app.constants.OperatorValue;
import com.konnect.app.domain.NetworkDAO;
import com.konnect.app.domain.NotificationDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.Network;
import com.konnect.app.pojo.response.ErrorCodes;
import com.konnect.app.tomcat.api.NetworkService;
import com.konnect.app.tomcat.api.manager.NetworkManager;
import com.konnect.app.tomcat.api.manager.UpdateNetworkImage;
import com.konnect.app.utils.SolrQueryUtils;

@Service("networkService")
public class NetworkServiceImpl implements NetworkService {

    @Autowired
    private NetworkDAO networkDAO;

    @Autowired
    private ProfileDAO profileDAO;

    @Autowired
    private NetworkManager networkManager;

    @Autowired
    private UpdateNetworkImage updateNetworkImage;

    @Autowired
    private NotificationDAO notificationDAO;

    @Autowired
    private Environment envProp;

    @Override
    public Response getMentors(String profileId) {
        List<Network> networks;
        Map<String,String> searchParams = getSearchparams(profileId, "menteeId");
        try {
            networks = networkDAO.filter(-1, -1, searchParams, "createdTime", true);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }

        GenericEntity<List<Network>> entity = new GenericEntity<List<Network>>(networks) {};

        return Response.ok(entity).build();
    }

    @Override
    public Response getMentee(String profileId) {
        List<Network> networks;
        Map<String,String> searchParams = getSearchparams(profileId, "mentorId");
        try {
            networks = networkDAO.filter(-1, -1, searchParams, "createdTime", true);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }

        GenericEntity<List<Network>> entity = new GenericEntity<List<Network>>(networks) {};

        return Response.ok(entity).build();
    }

    @Override
    public Response getPending(String profileId) {
        List<Network> networks;
        Map<String, String> searchParams = new HashMap<>();
        searchParams.put("status", NetworkStatus.Pending.name());
        searchParams.put("mentorId", SolrQueryUtils.quoteValue(profileId));
        try {
            networks = networkDAO.filter(-1, -1, searchParams, "menteeId", true);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }

        GenericEntity<List<Network>> entity = new GenericEntity<List<Network>>(networks) {};

        return Response.ok(entity).build();
    }

    @Override
    public Response postRequest(Network network) {

        String status = network.getStatus();

        if (org.apache.commons.lang3.StringUtils.equals("Pending", status)) {
            ErrorCodes error = networkManager.preCheck(network.getMentorId(), network.getMenteeId());
            if (error != null) {
                return Response.serverError().status(400).entity(error.getResponse()).build();
            }

        }

        networkDAO.insert(network);

        networkManager.postNotification(network);
        return Response.ok(network).build();
    }


    @Override
    public Response delete(String id) {
        networkDAO.delete(id);
        return Response.ok().build();
    }


    @Override
    public Response updateImageUrl(String profileId, String imageUrl) {
        List<Network> networksMentor;
        List<Network> networksMentee;
        Map<String, String> searchParamsMentor = getSearchparams(profileId,"mentorId");
        Map<String, String> searchParamsMentee = getSearchparams(profileId,"menteeId");
        /* Change ImageUrl in Mentee Requests */

        try {
            networksMentor = networkDAO.filter(-1, -1, searchParamsMentor, "createdTime", true);
            networksMentor.forEach((connection) -> {
                if (StringUtils.isNotEmpty(imageUrl)) {
                    connection.setMentorImg(imageUrl);
                    networkDAO.insert(connection);
                }
            });
            /* Change ImageUrl in Mentor Requests */
            networksMentee = networkDAO.filter(-1, -1, searchParamsMentee, "createdTime", true);
            networksMentee.forEach((connection) -> {
                if (StringUtils.isNotEmpty(imageUrl)) {
                    connection.setMenteeImg(imageUrl);
                    networkDAO.insert(connection);
                }
            });
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }

        // TODO Auto-generated method stub
        return Response.ok().build();
    }

    public HashMap<String, String> getSearchparams(String profileId, String field) {
        Map<String, String> searchParams = new HashMap<>();
        searchParams.put(field, SolrQueryUtils.quoteValue(profileId));
        searchParams.put("status",
                        "(" + NetworkStatus.Approved.name() + " OR " + NetworkStatus.Pending.name() + ")");
        return (HashMap<String, String>) searchParams;
    }

    @Override
    public Response getNetworkList(String profileId) {
        List<Network> networksMentor = new ArrayList<>();
        List<Network> networksMentee = new ArrayList<>();
        Map<String,String> searchParamsMentor = getSearchparams(profileId, "menteeId");
        Map<String,String> searchParamsMentee = getSearchparams(profileId, "mentorId");

        try {
            networksMentor = networkDAO.filter(100, 0, searchParamsMentor, "createdTime", true);
            networksMentee = networkDAO.filter(100, 0, searchParamsMentee, "createdTime", true);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Map<String, Network> dict = new HashMap<>();

        networksMentor.stream().forEach(network -> {
            dict.put(network.getMentorId(), network);
        });

        networksMentee.stream().forEach(network -> {
            dict.put(network.getMenteeId(), network);
        });

        Map<String, LinkedInProfile> profileList = profileDAO.batchGet("id", dict.keySet());

        profileList.forEach((id, profile) -> {
            Network network = dict.get(id);
            network.setFirstName(profile.getFirstName());
            network.setLastName(profile.getLastName());
        });

        Map<String, List<Network>> result = new HashMap<>();
        result.put("mentors", networksMentor);
        result.put("mentees", networksMentee);

        return Response.ok(result).build();
    }

    @Override
    public Response updateall(String profilId, String imageUrl) {
        //        updateNetworkImage.update();

        return Response.ok().build();
    }

    @Override
    public Response getSuggestions(String profileId) {
        List<LinkedInProfile> suggestions;
        try {
            suggestions = profileDAO.filter(50, 0, "-id", SolrQueryUtils.quoteValue(profileId), null, false);
        } catch (SolrServerException | IOException e) {
            System.out.println("unable to get suggestions + " + e.getMessage());
            return Response.serverError().entity("Unable to get suggestions").status(Status.INTERNAL_SERVER_ERROR).build();

        }

        return Response.ok(suggestions).build();
    }

    public Response _getSuggestions(String profileId) {
        List<LinkedInProfile> suggestions = Collections.EMPTY_LIST;
        try {
            Map<String,Operator> keyOpMap = prepareKeyOperatorMap();
            List<Network> networks = networkDAO.getNetworks(profileId);
            LinkedInProfile profile = profileDAO.getProfile(profileId);
            Map<String,OperatorValue> valuesMap = new HashMap<>();


            if(CollectionUtils.isEmpty(networks)){
                addKeyToValueMap(valuesMap,keyOpMap,profile,"");

            }else{
                addKeyToValueMap(valuesMap,keyOpMap,profile,"");
                List<String> contacts = new ArrayList<>();
                networks.forEach((network)->{
                    contacts.add(network.getMenteeId());
                    contacts.add(network.getMentorId());
                });
                valuesMap.put("id", new OperatorValue(Operator.NOTIN, contacts));
            }
            suggestions = profileDAO.getSuggestions(valuesMap);


        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
        return Response.ok(suggestions).build();
    }

    public void addKeyToValueMap(Map<String, OperatorValue> valuesMap,Map<String,Operator> keyOpMap,LinkedInProfile profile,String... skip) {
        keyOpMap.keySet().forEach((key)->{
            if(!ArrayUtils.contains(skip, key)){
                List<String> values=null;
                boolean go = true;
                if(keyOpMap.get(key) == Operator.ALGO){
                    Algorithm lgo = Algorithms.get(key);
                    values = new ArrayList(){{add(lgo.apply(profile));}};
                    go = false;
                }
                if(go){
                    switch(key){
                    case "tags" : values=profile.getTags();
                    break;
                    case "id":    values=new ArrayList(){{add(profile.getId());}};
                    break;
                    case "firstName": values=new ArrayList(){{add(profile.getFirstName());}};
                    break;
                    case "formattedName": values=new ArrayList(){{add(profile.getFormattedName());}};
                    break;
                    case "headline": values=new ArrayList(){{add(profile.getHeadline());}};
                    break;
                    case "industry": values=new ArrayList(){{add(profile.getIndustry());}};
                    break;

                    case "location": values=new ArrayList(){{add(profile.getLocation());}};
                    break;
                    case "summary": values=new ArrayList(){{add(profile.getSummary());}};
                    break;
                    case "currentOccupation": values=new ArrayList(){{add(profile.getCurrentOccupation());}};
                    break;
                    case "prevOccupation": values=new ArrayList(){{add(profile.getPrevOccupation());}};
                    break;
                    case "education": values=new ArrayList(){{add(profile.getEducation());}};
                    break;
                    case "experienceLevel": values=new ArrayList(){{add(profile.getExperienceLevel());}};
                    break;

                    case "affiliations": values=profile.getAffiliations();
                    break;

                    }
                }
                valuesMap.put(key, new OperatorValue(keyOpMap.get(key),values));
            }
        });
    }

    public Map<String, Operator> prepareKeyOperatorMap() {
        Map<String,Operator> keyOpMap = new HashMap<>();
        String propKeys = envProp.getProperty("api.suggestion.keys");
        String[] keys = propKeys.split(",");
        for(String key:keys){
            String operator = envProp.getProperty("api.suggestion."+key+".operator");
            keyOpMap.put(key,Operator.valueOf(operator));
        }

        return keyOpMap;
    }

	@Override
	public Response postRequest(String profile1, String profile2, String status) {
		Map<String, String> searchParams, searchParamsReversed;
		List<Network> networks;
		Network request = new Network();
		searchParams = new HashMap<String, String>() {
			{
				put("mentorId", profile1);
				put("menteeId", profile2);
			}
		};
		searchParamsReversed = new HashMap<String, String>() {
			{
				put("mentorId", profile2);
				put("menteeId", profile1);
			}
		};
		try {
			networks = networkDAO.filter(1, 0, searchParams, "status", false);
			if (networks.isEmpty()) {
				networks = networkDAO.filter(1, 0, searchParamsReversed, "status", false);
			}
			request = networks.get(0);
			request.setStatus(status);
			request.setUpdatedTime(new Date().getTime());
			networkDAO.insert(request);
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
		}
		
		return Response.ok().build();
	}

    /*	public static void main(String arsg[]){
		Map<String,Operator> keyOpMap = prepareKeyOperatorMap();
		Map<String,OperatorValue> valuesMap = new HashMap<>();
		LinkedInProfile profile = new LinkedInProfile();
		profile.setExperienceLevel(ExperienceLevel.EntryLevel.name());
		profile.setTags(new ArrayList(){{add("computer");add("product management");add("data-science");}});
		addKeyToValueMap(valuesMap,keyOpMap,profile,"");
	}*/
}

