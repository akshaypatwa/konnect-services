package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.ArticleDAO;
import com.konnect.app.domain.BookmarkDAO;
import com.konnect.app.domain.NotificationDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.message.ApplePushNotification;
import com.konnect.app.message.ApplePushNotification.Navigate;
import com.konnect.app.pojo.Article;
import com.konnect.app.pojo.Bookmark;
import com.konnect.app.pojo.DataStoreResponse;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.Notification;
import com.konnect.app.tomcat.api.BookmarkService;
import com.konnect.app.utils.SolrQueryUtils;

@Service("bookmarkService")
public class BookmarkServiceImpl implements BookmarkService {

    @Autowired
    private BookmarkDAO bookmarkDAO;

    @Autowired
    private ArticleDAO articleDao;

    @Autowired
    private ProfileDAO profileDAO;
    
    @Autowired
    private NotificationDAO notificationDAO;
    
    @Autowired
    private ApplePushNotification pushNotification;

    @Override
    public Response create(Bookmark bookmark) {
        Bookmark oldBookmark = bookmarkDAO.get(bookmark.getProfileId(), bookmark.getArticleId());
        DataStoreResponse resp = bookmarkDAO.insert(bookmark);

        String articleId = bookmark.getArticleId();
        Article article = null; article = articleDao.get("id", articleId);

        updateArticleHelpfulCount(oldBookmark, bookmark, article);

        GenericEntity<DataStoreResponse> entity = new GenericEntity<DataStoreResponse>(resp) {};
        return Response.ok().entity(entity).build();
    }

    private void updateArticleHelpfulCount(Bookmark oldBookmark, Bookmark bookmark, Article article) {
        if (article != null) {
            Integer numHelpful = article.getNumHelpful();
            if (numHelpful == null || numHelpful < 0) {
                numHelpful = 0;
            }

            if (oldBookmark == null && bookmark.getLiked()) {
                numHelpful = 1;
                sendPushNotification(bookmark);
            } else {
                if (hasBookmarked(oldBookmark, bookmark)) {
                    numHelpful++;
                }

                if (hasRemovedBookmarked(oldBookmark, bookmark)) {
                    numHelpful--;
                }
            }

            article.setNumHelpful(numHelpful);

            articleDao.update(article);
        }
    }

    private boolean hasBookmarked(Bookmark oldBookmark, Bookmark bookmark) {
        if (!oldBookmark.getLiked() && bookmark.getLiked()) {
            return true;
        }
        return false;
    }

    private boolean hasRemovedBookmarked(Bookmark oldBookmark, Bookmark bookmark) {
        if (oldBookmark.getLiked() && !bookmark.getLiked()) {
            return true;
        }
        return false;
    }
    
    @Override
    public Response getForProfileId(String profileId) {
        List<Bookmark> bookmarks;
        Map<String, Bookmark> bookMap = new HashMap<>();
        Map<String, Article> articles = new HashMap<>();

        try {
            bookmarks = bookmarkDAO.filter(20, 0, "profileId", SolrQueryUtils.quoteValue(profileId), "timestamp", false);
            if (!bookmarks.isEmpty()) {
                bookmarks.forEach((item) -> {
                    if (item.getArticleId() != null && item.getSaved()) {
                        bookMap.put(item.getArticleId(), item);
                    }
                });
            }
            articles = articleDao.batchGet("id", bookMap.keySet());
            Map<String, Article> attachedList = stitch(articles, bookmarks);
            Map<String, List<Article>> result = arrange(attachedList);
            return Response.ok(result).build();
            //        articles = articleDao.groupBy(-1, -1, searchParams, "publishedTime", true, "category");
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }
        //        GenericEntity<List<Bookmark>> entity = new GenericEntity<List<Bookmark>>(bookmarks) {};


        //        return Response.ok(result).build();
    }

    private Map<String, Article> stitch(Map<String, Article> articles, List<Bookmark> bookmarks) {
        for (Bookmark bookmark: bookmarks) {
            String articleId = bookmark.getArticleId();

            if (articleId != null && articles.containsKey(articleId)) {
                Article article = articles.get(articleId);

                article.setLiked(bookmark.getLiked());
                article.setSaved(bookmark.getSaved());
                article.setShared(bookmark.getShared());
                article.setRead(bookmark.getRead());
            }
        }
        return articles;
    }

    private Map<String, List<Article>> arrange(Map<String, Article> articles) {
        Map<String, List<Article>> articleResult = new HashMap<String, List<Article>>();

        for (Article article: articles.values()) {
            if (articleResult.containsKey(article.getCategory())) {
                articleResult.get(article.getCategory()).add(article);
            } else {
                List<Article> list = new ArrayList<Article>();
                list.add(article);
                articleResult.put(article.getCategory(), list);
            }
        }
        return articleResult;
    }
    
    private void sendPushNotification(Bookmark bookmark) {
    	String articleId = bookmark.getArticleId();
    	String name = "";
    	String message = "";
    	Notification notification = new Notification();
    	LinkedInProfile fromProfile;
    	LinkedInProfile toProfile;
    	try {
			Article article = articleDao.get("id", articleId);
			fromProfile = profileDAO.getProfile(bookmark.getProfileId());
			toProfile = profileDAO.getProfile(article.getPublishedProfileId());
			name = fromProfile.getFormattedName() != null ? fromProfile.getFormattedName() : fromProfile.getFirstName() + " " + fromProfile.getLastName(); 
			
			message = name + " liked your article.";
			notification.setFromId(fromProfile.getId());
	        notification.setFromPictureURL(fromProfile.getPictureUrl());
	        notification.setToId(toProfile.getId());
	        notification.setEventTime(new Date().getTime());
	        notification.setType("Article");
	        notification.setMessage(message);
	        notification.setRead(false);
	        notification.setPayload("");
	        System.out.println("Sending Notification");
	        notificationDAO.insert(notification);

            if (StringUtils.isNotBlank(toProfile.getDeviceToken())) {
                pushNotification.sendAsync(toProfile.getDeviceToken(), message, "Like!", fromProfile.getId(), Navigate.Home);
            }
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
}
