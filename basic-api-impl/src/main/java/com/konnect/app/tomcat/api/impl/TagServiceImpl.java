package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.TagDAO;
import com.konnect.app.pojo.Tag;
import com.konnect.app.sync.registry.TagSource;
import com.konnect.app.sync.registry.TagSourceRegistry;
import com.konnect.app.tomcat.api.TagService;

@Service("tagService")
public class TagServiceImpl implements TagService,TagSource {

    @Autowired
    private TagDAO tagDAO;

    private TagSourceRegistry tagRegistry;


    public TagServiceImpl() {
        super();
        //register();
    }

    @Autowired
    public TagServiceImpl(TagSourceRegistry tagRegistry){
        this.tagRegistry = tagRegistry;
        register();
    }
    @Override
    public Response getTag(String id) {
        Tag tag = null;
        try {
            tag = tagDAO.get(id);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return Response.ok().entity(tag).build();
    }

    @Override
    public Response getTags(int limit, int offset) {
        List<Tag> tags = null;
        try {
            tags = tagDAO.getAll(limit, offset);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        GenericEntity<List<Tag>> entity = new GenericEntity<List<Tag>>(tags) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    public Response create(Tag tag) {
        tagDAO.insert(tag);
        return Response.ok(tag).build();
    }

    @Override
    public Response update(Tag tag) {
        tagDAO.insert(tag);
        return Response.ok().build();
    }

    @Override
    public Response delete(String id) {
        tagDAO.delete(id);
        return Response.ok().build();
    }

    @Override
    public List<String> getTags() {
        List<Tag> tags;
        List<String> tagStrings = new ArrayList<>();
        try {
            tags = tagDAO.getAll(1000, 0);
            tags.forEach((tag) -> {
                List<String> ind = tag.getIndustry() != null ? tag.getIndustry() : new ArrayList<String>();
                ind.forEach(t->{
                    tagStrings.add(t+" "+tag.getName());
                });
            });
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
        return tagStrings;
    }

    @Override
    public void register() {
        tagRegistry.register(this);
    }

    @Override
    public Response search(String s) {
        List<Tag> tags = null;
        try {
            tags = tagDAO.searchByField("name", s);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }
        GenericEntity<List<Tag>> entity = new GenericEntity<List<Tag>>(tags) {};

        return Response.ok(entity).build();
    }

}
