package com.konnect.app.tomcat.api.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.attachment.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.konnect.app.scripts.S3Utils;
import com.konnect.app.tomcat.api.FileService;
import com.konnect.app.utils.ImageUtils;

@Service("fileService")
public class FileServiceImpl implements FileService {

    @Autowired
    private S3Utils s3Utils;

    @Autowired
    private ImageUtils imageUtils;

    @Autowired
    private Environment envProp;

    @Override
    public Response upload(Attachment attachment) {

        String filen = UUID.randomUUID().toString() ;

        try {
            InputStream is = attachment.getDataHandler().getInputStream();
            compressAndUpload(is, filen);

        } catch (IOException e) {
            e.printStackTrace();

        }

        FileUploadResponse response = new FileUploadResponse();
        response.setFileName(StaticServiceImpl.S3_BUCKET + filen);

        return Response.ok().entity(response).build();
    }

    private void compressAndUpload(InputStream is, String filen) {
        s3Utils.putResource(filen, is);
    }

    @Override
    public Response uploadArtifact(Attachment attachment) {

        ContentDisposition cd = attachment.getContentDisposition();
        if (attachment != null && cd != null && StringUtils.isNotBlank(cd.getParameter("filename"))) {
            String filename = cd.getParameter("filename");
            if (filename.endsWith("ipa")) {
                FileUploadResponse response = new FileUploadResponse();
                response.setFileName("http://ec2-34-212-130-91.us-west-2.compute.amazonaws.com:8080/static/artifacts/" + filename);
                return Response.ok().entity(response).build();
            }
        }

        return Response.serverError().entity(new IllegalArgumentException("Filename doesnt appear to be correct")).build();
    }


    @Override
    public Response getTimestamp(String filename) {
        String filePath =  filename;
        File file = new File(filePath);
        long modified = file.lastModified();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(modified);

        DateFormat format = SimpleDateFormat.getDateTimeInstance();
        String timestamp = format.format(calendar.getTime());
        return Response.ok(timestamp).build();
    }

    @XmlRootElement
    public static class FileUploadResponse {
        private String fileName;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }
    }

}
