package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.konnect.app.constants.Badge;
import com.konnect.app.domain.GoalDAO;
import com.konnect.app.domain.NotificationDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.message.ApplePushNotification;
import com.konnect.app.message.ApplePushNotification.Navigate;
import com.konnect.app.pojo.DataStoreResponse;
import com.konnect.app.pojo.Goal;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.Notification;
import com.konnect.app.pojo.response.GoalResponse;
import com.konnect.app.tomcat.api.GoalService;
import com.konnect.app.utils.SolrQueryUtils;

@Service("goalService")
public class GoalServiceImpl implements GoalService {

    private static final Logger logger = LoggerFactory.getLogger(GoalServiceImpl.class);

    @Autowired
    private GoalDAO goalDAO;

    @Autowired
    private ProfileDAO dao;

    @Autowired
    private NotificationDAO notificationDAO;

    @Autowired
    private ApplePushNotification pushNotification;

    @Override
    public Response getGoals(String profileId) {
        List<Goal> goals = null;
        logger.info("Getting goals for", profileId);
        try {
            goals = goalDAO.filter(100, 0, "menteeId", profileId);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }

        GoalResponse response = new GoalResponse();

        if (CollectionUtils.isNotEmpty(goals)) {
            Map<String, List<Goal>> responseMap = goals.stream().collect(
                            Collectors.groupingBy((Goal g) -> g.getArea() == null ? "" : g.getArea(), Collectors.toList()));
            response.setGoals(responseMap);
        }

        return Response.ok(response).build();
    }

    @Override
    public Response getGoalsOfMentees(String profileId) {
        List<Goal> goals = null;
        String safeProfileId = SolrQueryUtils.quoteValue(profileId);

        Map<String, String> searchParams = new HashMap<String, String>() {
            {
                put("mentorIds", safeProfileId);
                put("-menteeId", safeProfileId);
            }
        };
        try {
            // goals = goalDAO.filter(100, 0, "mentorIds", profileId);
            goals = goalDAO.filter(100, 0, searchParams, "id", false);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }

        GoalResponse response = new GoalResponse();

        if (CollectionUtils.isNotEmpty(goals)) {
            Map<String, List<Goal>> responseMap = goals.stream().filter(g -> g.getMenteeId() != null)
                            .collect(Collectors.groupingBy((Goal g) -> g.getMenteeId() == null ? "" : g.getMenteeId(), Collectors.toList()));
            response.setGoals(responseMap);
        }

        return Response.ok(response).build();
    }

    @Override
    public Response create(Goal goal) {
        boolean isNew = goal.getId() == null;
        if (!goal.getStatus().equals("Completed")) {
            goal.setStarsCount(0);
            goal.setFeedBackText("");
        } else if (goal.getStatus().equals("Completed") && goal.getStarsCount() > 3) {
            String badgeCategory = new Badge(goal.getArea()).getBadgeTitle();
            try {
                LinkedInProfile profile = dao.getProfile(goal.getMentorId().get(0));
                Map<String, Integer> map;
                com.konnect.app.pojo.Badge bg;
                Gson gson = new Gson();
                if (profile.getBadges() != null) {
                    // Convert string to java Object change values and send back
                    // as string.
                    String str = profile.getBadges();
                    bg = gson.fromJson(str, com.konnect.app.pojo.Badge.class);
                    map = bg.getBadge(); // get map of previous badges
                    int prevCount = map.get(badgeCategory) + 1; // increase
                    // count of
                    // relevant
                    // badge
                    map.put(badgeCategory, prevCount);
                    bg.setBadge(map); // set new map values
                    profile.setBadges(bg.getString()); // set badge string back
                    // to profile

                } else {
                    bg = new com.konnect.app.pojo.Badge();
                    map = bg.getBadge();
                    map.put(badgeCategory, 1);
                    bg.setBadge(map);
                    profile.setBadges(bg.getString());
                }
                dao.insert(profile);
            } catch (SolrServerException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            postNotification(goal, "testimonialRequest", goal.getMenteeId(), goal.getMentorId().get(0));
        }

        DataStoreResponse resp = goalDAO.insert(goal);
        GenericEntity<DataStoreResponse> entity = new GenericEntity<DataStoreResponse>(resp) {
        };

        if (!goal.getMentorId().contains(goal.getMenteeId()) && isNew) {
            if (!goal.getMenteeId().equals(goal.getProfileId())) {
                postNotification(goal, "toMentee", goal.getProfileId(), goal.getMenteeId());
            } else {
                goal.getMentorId().forEach((mentorId) -> {
                    postNotification(goal, "toMentor", goal.getProfileId(), mentorId);
                });
            }

        } else {
            sendPushNotification(goal);
        }

        return Response.ok().entity(entity).build();
    }

    private void sendPushNotification(Goal goal) {
        List<String> toIds = goal.getMenteeId().equals(goal.getProfileId()) ? goal.getMentorId()
                        : Collections.singletonList(goal.getMenteeId());

        for (String toId : toIds) {
            try {
                LinkedInProfile toProfile = dao.getProfile(toId);

                // mentee updated his own goal (the definition of goal.getProfileId() is still unclear)
                if (goal.getProfileId().equals(goal.getMenteeId())) {
                    pushNotification.sendAsync(toProfile.getDeviceToken(),
                                    goal.getProfileFirstName() + " updated his goal.", "Goal!", toId, Navigate.Goal);
                } else {
                    // mentor updated mentee's goal

                    LinkedInProfile fromProfile = dao.getProfile(goal.getProfileId());
                    pushNotification.sendAsync(toProfile.getDeviceToken(),
                                    fromProfile.getFirstName() + " updated your goal.", "Goal!", toId, Navigate.Goal);

                }


            } catch (SolrServerException | IOException e) {
                logger.error("Error pushing goal notification", e);
            }

        }
    }

    private void postNotification(Goal goal, String sendTo, String fromId, String toId) {
        Notification notification = new Notification();
        LinkedInProfile profile;
        String message = "", fromPictureUrl;
        try {
            profile = dao.getProfile(fromId);
            if (sendTo.equals("toMentee")) {
                message = profile.getFirstName() + " added a new goal for you.";
            } else if (sendTo.equals("toMentor")) {
                message = "Your mentee " + profile.getFirstName() + " added a new goal.";
            } else if (sendTo.equals("testimonialRequest")) {
                message = profile.getFirstName() + " has written a testimonial for you.";
            }

            // String fromId = goal.getProfileId();
            fromPictureUrl = profile.getPictureUrl();
            // String toId = goal.getMenteeId();

            notification.setFromId(fromId);
            notification.setFromPictureURL(fromPictureUrl);
            notification.setToId(toId);
            notification.setEventTime(new Date().getTime());
            notification.setType("Goal");
            notification.setMessage(message);
            notification.setRead(false);
            notification.setPayload("");
            notificationDAO.insert(notification);

            LinkedInProfile destinationProfile = dao.getProfile(toId);

            if (StringUtils.isNotBlank(destinationProfile.getDeviceToken())) {
                pushNotification.sendAsync(destinationProfile.getDeviceToken(), message, "New Goal!", fromId, Navigate.Goal);
            }

        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public Response delete(String goalId) {
        goalDAO.delete(goalId);
        return Response.ok().build();
    }

    @Override
    public Response readAll(String profileId) {
        List<Goal> goals = null;
        List<Goal> goalsMentor = null;
        try {
            goals = goalDAO.filter(100, 0, "mentorIds", profileId);
            goalsMentor = goalDAO.filter(100, 0, "menteeId", profileId);
            for (Goal item : goals) {
                item.setRead(true);
                goalDAO.insert(item);
            }
            for (Goal goal : goalsMentor) {
                goal.setRead(true);
                goalDAO.insert(goal);
            }
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }
        return Response.ok().build();
    }

    private Map<String, List<Goal>> getGoalResponseMap(List<Goal> goals) {
        if (CollectionUtils.isNotEmpty(goals)) {
            Map<String, List<Goal>> responseMap = goals.stream().collect(
                            Collectors.groupingBy((Goal g) -> g.getArea() == null ? "" : g.getArea(), Collectors.toList()));
            return responseMap;
        }
        return null;
    }

    @Override
    public Response getAllGoals(String profileId) {
        List<Goal> goals = null;
        GoalResponse userGoals = new GoalResponse();
        GoalResponse menteeGoals = new GoalResponse();
        String safeProfileId = SolrQueryUtils.quoteValue(profileId);

        logger.info("Getting goals for", profileId);
        try {
            goals = goalDAO.filter(100, 0, "menteeId", profileId);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }
        userGoals.setGoals(getGoalResponseMap(goals));

        Map<String, String> searchParams = new HashMap<String, String>() {
            {
                put("mentorIds", safeProfileId);
                put("-menteeId", safeProfileId);
            }
        };
        try {
            goals = goalDAO.filter(100, 0, searchParams, "id", false);

            if (CollectionUtils.isNotEmpty(goals)) {
                Map<String, List<Goal>> responseMap = goals.stream()
                                .collect(Collectors.groupingBy((Goal g) -> g.getMenteeId() == null ? "" : g.getMenteeId(), Collectors.toList()));
                menteeGoals.setGoals(responseMap);
            }
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }
        //        menteeGoals.setGoals(getGoalResponseMap(goals));
        Map<String, GoalResponse> result = new HashMap<String, GoalResponse>() {
            {
                put("userGoals", userGoals);
                put("menteeGoals", menteeGoals);
            }
        };

        return Response.ok(result).build();
    }

    @Override
    public Response getGoalsWithFeedback(String profileId, String isOwnProfile) {
        Map<String, String> searchParams = new HashMap<String, String>();
        List<Goal> goalsList;
        boolean isOwner = Boolean.parseBoolean(isOwnProfile);
        searchParams.put("mentorIds", profileId);
        searchParams.put("starsCount", "[4 TO 5]");

        try {
            goalsList = goalDAO.filter(10, 0, searchParams, "", true);
            if (!isOwner) {
                goalsList = goalsList.stream().filter(goal -> goal.getAccepted()).collect(Collectors.toList());
            }
            for(Goal goal: goalsList) {
                LinkedInProfile menteeProfile = dao.getProfile(goal.getMenteeId());
                goal.setMenteeProfile(menteeProfile);
            }

            GenericEntity<List<Goal>> entity = new GenericEntity<List<Goal>>(goalsList) {};
            return Response.ok().entity(goalsList).build();
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
