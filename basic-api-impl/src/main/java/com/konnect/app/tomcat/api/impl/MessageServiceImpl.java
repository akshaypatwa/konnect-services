package com.konnect.app.tomcat.api.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.konnect.app.domain.MessageDAO;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.message.ApplePushNotification;
import com.konnect.app.message.ApplePushNotification.Navigate;
import com.konnect.app.message.EjabberdProxy;
import com.konnect.app.pojo.GroupResult;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.Message;
import com.konnect.app.pojo.DataStoreResponse;
import com.konnect.app.tomcat.api.MessageService;
import com.konnect.app.tomcat.api.manager.MessageManager;
import com.konnect.app.utils.SolrQueryUtils;

@Service("messageService")
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageDAO messageDao;

    @Autowired
    private ProfileDAO profileDao;

    @Autowired
    private MessageManager messageManager;

    @Autowired
    private ApplePushNotification pushNotification;

    @Override
    public Response getMessage(String id) {
        Message data = null; data = messageDao.get("id", id);

        return Response.ok().entity(data).build();
    }

    @Override
    public Response getMessageList(int limit, int offset, String fromId, String toId,String sort, String sortOrder) {
        List<Message> data = null;
        try {
            data = messageDao.getConversation(toId, fromId, limit, offset, sortOrder.equals("d"));
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }

        GenericEntity<List<Message>> entity = new GenericEntity<List<Message>>(data) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    public Response create(Message msg) {
        msg.setTimestamp(Calendar.getInstance().getTimeInMillis());
        DataStoreResponse resp = messageDao.insert(msg);
        EjabberdProxy ejProxy = new EjabberdProxy("ec2-54-200-51-71.us-west-2.compute.amazonaws.com",5222, msg.getFromId(), "chatPassword");
        ejProxy.sendMessage(msg.getMessage(), msg.getToId().toLowerCase() + "@ec2-54-200-51-71.us-west-2.compute.amazonaws.com");
        GenericEntity<DataStoreResponse> entity = new GenericEntity<DataStoreResponse>(resp) {};

        if (StringUtils.isNotBlank(msg.getToId())) {
            LinkedInProfile profile = null;
            try {
                profile = profileDao.getProfile(msg.getToId());
            } catch (SolrServerException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (profile != null) {
                pushNotification.sendAsync(profile.getDeviceToken(), msg.getMessage(), "Message from " + msg.getFromName(), msg.getFromId(), Navigate.Message);
            }
        }
        return Response.ok().entity(entity).build();

    }

    @Override
    public Response update(Message msg) {
        messageDao.update(msg);
        return Response.ok().build();
    }

    @Override
    public Response delete(String id) {
        messageDao.delete(id);
        return Response.ok().build();
    }

    @Override
    public Response getAllMessages(String profileId, String sort, String sortOrder) {

        List<Message> messages;
        try {
            messages = messageDao.getMessages(SolrQueryUtils.quoteValue(profileId), 10, 0);
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return Response.serverError().entity(e).build();
        }

        Map<String, List<Message>> messageMap = new LinkedHashMap<>();

        if (CollectionUtils.isNotEmpty(messages)) {
            for (Message m: messages) {
                String id;
                if (m.getToId().equals(profileId)) {
                    id = m.getFromId();
                } else {
                    id = m.getToId();
                }

                if (id == null) {
                    id = "";
                }
                if (messageMap.containsKey(id)) {
                    messageMap.get(id).add(m);
                } else {
                    List<Message> messageList = new ArrayList<>();
                    messageList.add(m);
                    messageMap.put(id, messageList);
                }
            }
        }

        GenericEntity<Map<String, List<Message>>> entity = new GenericEntity<Map<String,List<Message>>>(messageMap){};
        return Response.ok().entity(entity).build();

    }

    @Override
    public Response getMessagesSummary(String fromId, Integer limit,Integer offset,String sort, String sortOrder) {
        Map<String, Object> searchParams = new HashMap<String,Object>()
        {
            private static final long serialVersionUID = 1L;

            {
                put("toId", SolrQueryUtils.quoteValue(fromId));
                put("read","false");
            }
        };

        List<GroupResult> results = messageDao.messagesFilterAndGroup(limit, offset, searchParams , "toId", sort, sortOrder);
        GenericEntity<List<GroupResult>> entity = new GenericEntity<List<GroupResult>>(results) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    public Response getMessagesUnread(String fromId, int limit, int offset, String sort, String sortOrder) {
        Map<String, Object> searchParams = new HashMap<String,Object>(){{put("fromId",SolrQueryUtils.quoteValue(fromId));
        put("read",false);}};
        // put("read","false");}};
        List<GroupResult> results = messageDao.messagesFilterAndGroup(limit, offset, searchParams , "toId", sort, sortOrder);
        GenericEntity<List<GroupResult>> entity = new GenericEntity<List<GroupResult>>(results) {};
        return Response.ok().entity(entity).build();
    }

    @Override
    public Response readAll(String fromId, String toId) {
        Map<String, String> searchParams = new HashMap<String,String>(){{
            put("fromId", SolrQueryUtils.quoteValue(fromId));
            put("toId", SolrQueryUtils.quoteValue(toId));
            put("read", "false");}};
            List<Message> result = new ArrayList<>();
            try {
                result = messageDao.filter(-1, -1, searchParams, "timestamp", false);
                result.forEach((message) -> {
                    message.setRead(true);
                    messageDao.insert(message);
                });
            } catch (SolrServerException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return Response.ok().build();
    }

    @Override
    public Response getMessageSummaryList(String profileId) {
        List<Message> messageList = messageManager.getSummaryList(SolrQueryUtils.quoteValue(profileId));
        GenericEntity<List<Message>> entity = new GenericEntity<List<Message>>(messageList) {};
        return Response.ok(entity).build();
    }
}
