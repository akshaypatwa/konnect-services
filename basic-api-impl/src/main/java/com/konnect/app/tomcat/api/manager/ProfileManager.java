package com.konnect.app.tomcat.api.manager;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.tomcat.api.impl.ProfileServiceImpl;

/**
 * Middle layer for {@link ProfileServiceImpl}.
 *
 * @author kazz
 *
 */
@Component
public class ProfileManager {

    @Autowired
    private ProfileDAO profileDao;


    public boolean updateDeviceToken(String profileId, String deviceToken) {
        LinkedInProfile profile;
        try {

            profile = profileDao.getProfile(profileId);

        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
            return false;
        }

        if (profile != null) {
            profile.setDeviceToken(deviceToken);
            profileDao.update(profile);
        }

        return true;

    }

}
