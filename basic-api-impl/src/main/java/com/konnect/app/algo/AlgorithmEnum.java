package com.konnect.app.algo;

public enum AlgorithmEnum {
	
		  EXPERIENCELEVEL(new ExperienceLevelAlgorithm());
	     Algorithm algo;
	     AlgorithmEnum(Algorithm algorithm){
	    	 algo = algorithm;
	     }
	     Algorithm getAlgo(){
	    	 return algo;
	     }
	     
}
