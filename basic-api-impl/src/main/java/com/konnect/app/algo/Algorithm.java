package com.konnect.app.algo;

public interface Algorithm<T> {
  public String apply(T object);
  
}
