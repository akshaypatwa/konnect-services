package com.konnect.app.algo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.konnect.app.constants.ExperienceLevel;
import com.konnect.app.pojo.LinkedInProfile;

public class ExperienceLevelAlgorithm implements Algorithm<LinkedInProfile> {

	@Override
	public String apply(LinkedInProfile profile) {
		List<ExperienceLevel> levs = new ArrayList<>();
		
			 ExperienceLevel[] levels = ExperienceLevel.values() ;
			 for(ExperienceLevel l : levels){
				 if(l.toString().equals(profile.getExperienceLevel())){
					 if(profile.getMentee()){
						 return getQuery(getExpLevels("More",l));
					 }
					 else if(profile.getMentor()){
						 return getQuery(getExpLevels("Less",l));
					 }
				 }
			 }
		return "";	
	}

	private String getQuery(List<ExperienceLevel> expLevels) {
		StringBuffer buf = new StringBuffer();
		buf.append("(");
		expLevels.forEach((exp)->{
			buf.append("experienceLevel:");
			buf.append(exp.toString()).append(" OR ");
		});
		String q = buf.toString();
		
		String temp = StringUtils.stripEnd(q," OR ");
		return temp+")";
	}

	private List<ExperienceLevel> getExpLevels(String moreOrLess, ExperienceLevel l) {
		List<ExperienceLevel> exps = new ArrayList<>();
			for(ExperienceLevel lev : ExperienceLevel.values()){
				if(StringUtils.equalsIgnoreCase(moreOrLess, "More")){
					if(lev.code() > l.code()){
						exps.add(lev);
					}
				}else if(StringUtils.equalsIgnoreCase(moreOrLess, "Less")){
					if(lev.code() < l.code()){
						exps.add(lev);
					}
				}
			}
		return exps;
	}
	
	/*public static void main(String[] s){
		 ExperienceLevel[] levels = ExperienceLevel.values() ;
		 for(ExperienceLevel l : levels){
			 if(l.toString().equals("Leader (Director & above)")){
				 System.out.println(getExpLevels("More",l));
			 }
		 }
	}*/
		

}
