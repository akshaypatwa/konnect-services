package com.konnect.app.algo;

import com.konnect.app.pojo.LinkedInProfile;

public class Algorithms {
	 
	public static Algorithm get(String key){
		return AlgorithmEnum.valueOf(key.toUpperCase()).getAlgo();
	}
	
	public static void main(String args[]){
		Algorithm lgo = get("experienceLevel");
		System.out.println(lgo.apply(new LinkedInProfile()));
	}
	
}
