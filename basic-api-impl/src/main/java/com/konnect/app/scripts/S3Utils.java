package com.konnect.app.scripts;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.Permission;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.konnect.app.domain.ProfileDAO;

@Component
public class S3Utils {

    private static final String BUCKET = "com.myvaxa.resources";

    @Autowired
    private Environment envProp;

    @Autowired
    private ProfileDAO profileDAO;

    public InputStream getResource(String name) {
        AmazonS3 s3client = getClient();

        try {
            S3Object s3Object = s3client.getObject(BUCKET, name);

            if (s3Object != null) {
                return s3Object.getObjectContent();
            }
        } catch (AmazonS3Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public void putResource(String name, InputStream input) {
        AmazonS3 s3client = getClient();

        AccessControlList acl = new AccessControlList();
        acl.grantPermission(GroupGrantee.AllUsers, Permission.Read);
        try {
            File file = File.createTempFile(name, "");
            FileUtils.copyInputStreamToFile(input, file);


            PutObjectRequest request = new PutObjectRequest(BUCKET, name, file);
            request.setAccessControlList(acl);
            s3client.putObject(request);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void migrateProfileImages() throws SolrServerException, IOException {

        //        profileDAO.getAll(20, 0, null, null);

        AmazonS3 s3client = getClient();

        s3client.listBuckets().stream().forEach(System.out::println);

        profileDAO.getAll(200, 0, null, null).stream().forEach((profile) -> {
            System.out.println(profile.getPictureUrl());
        });
    }


    private AmazonS3 getClient() {
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(envProp.getProperty("aws.accessKeyId"), envProp.getProperty("aws.secretKey"));


        AmazonS3 s3client = AmazonS3ClientBuilder.standard()
                        .withRegion(Regions.US_WEST_2)
                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                        .build();

        return s3client;
    }

}
