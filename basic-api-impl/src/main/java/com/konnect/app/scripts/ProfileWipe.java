package com.konnect.app.scripts;

import org.springframework.beans.factory.annotation.Autowired;

import com.konnect.app.domain.BookmarkDAO;
import com.konnect.app.domain.GoalDAO;
import com.konnect.app.domain.NetworkDAO;
import com.konnect.app.domain.ProfileDAO;

public class ProfileWipe {

    @Autowired
    private ProfileDAO profileDao;

    @Autowired
    private NetworkDAO networkDao;

    @Autowired
    private GoalDAO goalDao;

    @Autowired
    private BookmarkDAO bookmarkDao;

    public static void main(String args[]) {
        String profileId = "";

        ProfileWipe profileWipe = new ProfileWipe();

        profileWipe.wipe();

    }

    public void wipe() {
        if (profileDao == null) {
            System.out.println("null");
        }

        System.out.println("end");
    }

}
