package com.konnect.app.sync.registry;

import java.util.List;

public interface TagSource {
	public void register();
	public List<String> getTags();
	
}
