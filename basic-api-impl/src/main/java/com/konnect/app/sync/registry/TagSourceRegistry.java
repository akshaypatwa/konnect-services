package com.konnect.app.sync.registry;

import java.util.List;

public interface TagSourceRegistry {
	public void register(TagSource source);
	public List<String> getTags();
}
