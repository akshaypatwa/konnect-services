package com.konnect.app.sync.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class TagSourceRegistryImpl implements TagSourceRegistry {
	
	private List<TagSource> tagSources = new ArrayList<>();
	
	@Override
	public void register(TagSource source) {
		tagSources.add(source);
	}

	@Override
	public List<String> getTags() {
		List<String> tags = new ArrayList<>();
		for(TagSource tag : tagSources){
			
			tags.addAll(tag.getTags());
		}
		return tags;
	}
	
	
}
