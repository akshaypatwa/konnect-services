package org.springapp;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.konnect.app.AppConfig;
import com.konnect.app.domain.ProfileDAO;
import com.konnect.app.pojo.LinkedInProfile;

public class DynamoDBMigration {

    public static void main(String args[]) {

        try {
            (new DynamoDBMigration()).migrate();
        } catch (SolrServerException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e);
        } finally {
        }

        System.exit(0);
    }

    public void migrate() throws SolrServerException, IOException {
        ApplicationContext context =
                        new AnnotationConfigApplicationContext(AppConfig.class);

        Environment envProp = (Environment) context.getBean("envProp");

        BasicAWSCredentials awsCreds = new BasicAWSCredentials(envProp.getProperty("aws.accessKeyId"), envProp.getProperty("aws.secretKey"));

        AmazonDynamoDB dynamo = AmazonDynamoDBClientBuilder.standard()
                        .withRegion(Regions.US_WEST_2)
                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                        .build();


        ProfileDAO profileDao = context.getBean(ProfileDAO.class);
        List<LinkedInProfile> profiles = profileDao.getAll(100, 0, null, null);
        DynamoDBMapper mapper = new DynamoDBMapper(dynamo);

        for (LinkedInProfile profile: profiles) {
            System.out.println("creating - " + profile.getFirstName());
            mapper.save(profile);
            System.out.println("created - " + profile.getFirstName());
        }

        ((ConfigurableApplicationContext)context).close();
    }

    private void createColumns(Object object) {

    }
}
