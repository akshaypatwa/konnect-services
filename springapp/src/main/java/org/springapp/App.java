package org.springapp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.konnect.app.AppConfig;
import com.konnect.app.domain.ArticleDAO;
import com.konnect.app.domain.GoalDAO;
import com.konnect.app.domain.NetworkDAO;
import com.konnect.app.domain.ProfileDAO;

/**
 * Hello world!
 *
 */
public class App {

    public static final String S3_BUCKET = "https://s3-us-west-2.amazonaws.com/com.myvaxa.resources/";

    public static final String REPLACE = "https://rest.myvaxa.com/static/article-images/";

    public static void main(String[] args) {
        ApplicationContext context =
                        new AnnotationConfigApplicationContext(AppConfig.class);


        GoalDAO goalDao = context.getBean(GoalDAO.class);
        ProfileDAO profileDao = context.getBean(ProfileDAO.class);
        ArticleDAO articleDAO = context.getBean(ArticleDAO.class);
        NetworkDAO networkDao = context.getBean(NetworkDAO.class);
        //        MessageDAO articleDAO = context.getBean(MessageDAO.class);


        try {

            String[] ids = {"2reWPvBrxD"};

            for (String id: ids) {

                profileDao.delete(id);


            }



            //
            //
            //            for (LinkedInProfile goal: goals) {
            //
            //                String img = goal.getCoverImageUrl();
            //                if (StringUtils.contains(img, REPLACE)) {
            //                    img = StringUtils.replace(img, REPLACE, S3_BUCKET);
            //                    System.out.println(img);
            //                    goal.setCoverImageUrl(img);
            //                    profileDao.update(goal);
            //                }

            //                List<String> imgs = goal.getMentorImg();
            //
            //                if (CollectionUtils.isNotEmpty(imgs)) {
            //                    String[] imgArray = imgs.toArray(new String[]{});
            //                    boolean dirty = false;
            //
            //                    for (int i = 0; i < imgArray.length; i++) {
            //                        if (StringUtils.contains(imgArray[i], REPLACE)) {
            //                            dirty = true;
            //                            imgArray[i] = StringUtils.replace(imgArray[i], REPLACE, S3_BUCKET);
            //                        }
            //                    }
            //
            //                    if (dirty) {
            //                        List<String> newImgs = Arrays.asList(imgArray);
            //                        System.out.println(goal.getId());
            //                        System.out.println(newImgs);
            //                        System.out.println("-----");
            //
            //                        goal.setMentorImg(newImgs);
            //                        goalDao.update(goal);
            //                    }
            //                }


        } finally {
            ((ConfigurableApplicationContext)context).close();
        }


        System.exit(1);
    }
}
