package com.konnect.app.sync.adapter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.konnect.app.pojo.Article;
import com.webhoseio.sdk.WebhoseIOClient;

@Component
public class WebhoseAdapter implements FeedsAdapter<Article> {

	@Override
	public List<Article> syncFeeds(List<String> tags) {
		return Collections.EMPTY_LIST;
	}

	@Override
	public List<Article> syncFeeds(List<String> tags, Long lastCrawl) {
		WebhoseIOClient webhoseClient = WebhoseIOClient.getInstance("df52a21f-28b7-45c5-840d-533f02da954c");

	    List<Article> articles = new ArrayList<>();
	    Set<String> tagset = new HashSet<>(tags);
	    Long currentTime = System.currentTimeMillis();
	    for(String tag:tagset){
	    	Map<String, String> queries = new HashMap<String, String>();
	    	StringBuffer queryBuf = new StringBuffer("\"").append(tag).append("\" (site_type:news OR site_type:blogs) site:techcrunch.com language:english ");
	    	if(lastCrawl != null){
	    		queryBuf.append("crawled:>").append(lastCrawl);
	    	}
		    queries.put("q", queryBuf.toString());
			    queries.put("q", "\""+tag+"\" (site_type:news OR site_type:blogs) site:techcrunch.com language:english");
			// Fetch query result
	
			    JsonElement result;
				
			// Fetch query result

			try {
				result = webhoseClient.query("filterWebContent", queries);
				JsonObject resultJson = result.getAsJsonObject();
				
				int resultsCount = resultJson.get("totalResults").getAsInt();
				if(resultsCount == 0) continue;
				resultJson.getAsJsonArray("posts").forEach((post)->{
					Article article = new Article();
					article.setTags(new ArrayList<String>(){{add(tag);}});
					article.setCategory("News");
					article.setTitle(post.getAsJsonObject().get("thread").getAsJsonObject().get("title").getAsString());
					article.setLink(post.getAsJsonObject().get("thread").getAsJsonObject().get("url").getAsString());
					article.setImageURL(post.getAsJsonObject().get("thread").getAsJsonObject().get("main_image").getAsString());
					article.setSource("TechCrunch");
					article.setLastCrawlTime(currentTime);
					 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);
					 Date publishedDate = null;
					//Instant inst = Instant.parse(post.getAsJsonObject().get("thread").getAsJsonObject().get("published").getAsString()); 
					 try{
						  publishedDate = format.parse(post.getAsJsonObject().get("thread").getAsJsonObject().get("published").getAsString());
					 }catch(ParseException pe){
						 pe.printStackTrace();
						 
					 }
					article.setPublishedTime(publishedDate.getTime());
					articles.add(article);
				});
				// Print posts count
			} catch (URISyntaxException | IOException e) {
				e.printStackTrace();
			}
	    }
	return articles;
	}

}
