package com.konnect.app.sync;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.konnect.app.domain.ArticleDAO;
import com.konnect.app.pojo.Article;
import com.konnect.app.sync.adapter.FeedsAdapter;
import com.konnect.app.sync.registry.TagSourceRegistry;

@Component("feedSyncFacade")
public class FeedSyncFacadeImpl implements FeedSyncFacade {

    /*@Autowired
	@Qualifier(value="webhoseAdapter")
	private FeedsAdapter feedsAdapter;*/

    @Autowired
    @Qualifier(value="youtubeSearchAdapter")
    private FeedsAdapter<Article> youtubeSearchAdapter;

    @Autowired
    @Qualifier(value="techCrunchRSSAdapter")
    private FeedsAdapter<Article> techCrunchRSSAdapter;

    @Autowired
    @Qualifier(value="businessInsiderRSSAdapter")
    private FeedsAdapter<Article> businessInsiderRSSAdapter;

    @Autowired
    @Qualifier(value="bloombergRSSAdapter")
    private FeedsAdapter<Article> bloombergRSSAdapter;

    @Autowired
    private ArticleDAO articleDao;


    @Autowired
    private TagSourceRegistry tagSourceRegistry;

    @Override
    public void syncFeeds() {
        List<String> tags = tagSourceRegistry.getTags();
        List<Article> lastCrawledArticle = null;
        try {
            lastCrawledArticle = articleDao.getAll(1, 0, "lastCrawlTime", "d");
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
        Long lastCrawledTime = null;
        if(lastCrawledArticle != null && lastCrawledArticle.size()==1 ){
            lastCrawledTime = lastCrawledArticle.get(0).getLastCrawlTime();
        }
        //List<Article> articles = feedsAdapter.syncFeeds(tags,lastCrawledTime);
        List<Article> articles = new ArrayList<>();
        System.out.println("Updated " + articles.size() + "news feeds in database");
        List<Article> videos = youtubeSearchAdapter.syncFeeds(tags,lastCrawledTime);
        System.out.println("Updated " + videos.size() + "videos in database");
        List<Article> rssFeeds = techCrunchRSSAdapter.syncFeeds(tags, lastCrawledTime);
        List<Article> birssFeeds = businessInsiderRSSAdapter.syncFeeds(tags, lastCrawledTime);
        List<Article> blbrgFeeds = bloombergRSSAdapter.syncFeeds(tags, lastCrawledTime);
        System.out.println("Updated " + rssFeeds.size() + "rss feeds in database");
        articles.addAll(videos);
        articles.addAll(rssFeeds);
        articles.addAll(birssFeeds);
        articles.addAll(blbrgFeeds);
        if(CollectionUtils.isNotEmpty(articles))	{
            articleDao.insert(articles);
        }


    }

    @Override
    public List<Article> getFeeds(List<String> tags) {
        // TODO Auto-generated method stub
        return null;
    }

}
