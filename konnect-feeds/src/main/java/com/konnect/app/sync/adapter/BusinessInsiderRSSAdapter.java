package com.konnect.app.sync.adapter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Article;
import com.rometools.rome.feed.synd.SyndEntry;

@SuppressWarnings("deprecation")
@Component
public class BusinessInsiderRSSAdapter extends AbstractRSSAdapter<Article> {
	

    @SuppressWarnings("deprecation")
    @Override
    public List<Article> syncFeeds(List<String> tags, Long lastCrawl) {
       return super.syncFeeds(tags, lastCrawl);
    }

    
	@Override
	public String getRSSUrl() {
		return "http://feeds.feedburner.com/businessinsider?fmt=xml";
	}
	
	@Override
	public String getCategory() {
		return "News";
	}

	@Override
	public String getSource() {
		return "Business Insider";
	}

	@Override
	public void addThumbnail(SyndEntry entry, Article article) {
		for( Element fm : entry.getForeignMarkup()){
			String url = fm.getAttributeValue("url");
			
            if(StringUtils.isNotEmpty(url)){
                article.setImageURL(url);
                break;
            }
        }
		if(StringUtils.isEmpty(article.getImageURL())){
			article.setImageURL("");
		}
	}

	@Override
	public List<Article> getTypes() {
		return  new ArrayList<Article>();
	}

	@Override
	public Article getType() {
		return  new Article();
	}

}
