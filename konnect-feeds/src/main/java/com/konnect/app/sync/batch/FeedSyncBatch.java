package com.konnect.app.sync.batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.konnect.app.sync.FeedSyncFacade;

@Component
@EnableScheduling
public class FeedSyncBatch {
	
	@Autowired
	FeedSyncFacade feedSyncFacade;
	
	@Scheduled(cron="0 0 0 ? * *")
	public void run() {
		System.out.println("Batch running--");
		feedSyncFacade.syncFeeds();
	}
}
