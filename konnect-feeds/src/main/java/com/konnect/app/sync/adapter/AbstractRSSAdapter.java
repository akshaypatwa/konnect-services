package com.konnect.app.sync.adapter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.konnect.app.pojo.Article;
import com.rometools.fetcher.FeedFetcher;
import com.rometools.fetcher.FetcherException;
import com.rometools.fetcher.impl.HttpURLFeedFetcher;
import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;

public abstract class AbstractRSSAdapter<T extends Article> implements FeedsAdapter<T>{

    @Override
    public List<T> syncFeeds(List<String> tags) {
        return null;
    }

    @Override
    public List<T> syncFeeds(List<String> tags, Long lastCrawl) {
        List<T> articles= null;
        try{
            URL url = new URL(getRSSUrl());
            @SuppressWarnings("deprecation")
            FeedFetcher fetcher  = new HttpURLFeedFetcher();
            @SuppressWarnings("deprecation")
            SyndFeed feed = fetcher.retrieveFeed(url);
            List<SyndEntry> entries = feed.getEntries();
            articles = getTypes();
            Long currentTime = System.currentTimeMillis();
            for (SyndEntry entry : entries) {

                T article = getType();
                List<String> tagss = new ArrayList<>();
                for(SyndCategory synd : entry.getCategories()){
                    tagss.add(synd.getName());
                }
                article.setTags(tagss);
                article.setCategory(getCategory());
                article.setTitle(entry.getTitle());
                article.setLink(entry.getUri());
                addThumbnail(entry, article);
                article.setSource(getSource());
                article.setLastCrawlTime(currentTime);



                if (entry.getPublishedDate() != null) {
                    article.setPublishedTime(entry.getPublishedDate().getTime());
                }


                articles.add(article);
            }
            return articles;
        }
        catch( MalformedURLException me){
            me.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (FeedException e) {
            e.printStackTrace();
        } catch (@SuppressWarnings("deprecation") FetcherException e) {
            e.printStackTrace();
        }
        return Collections.EMPTY_LIST;
    }

    public abstract String getRSSUrl() ;
    public abstract String getCategory() ;
    public abstract String getSource() ;
    public abstract void addThumbnail(SyndEntry entry,T article);
    public abstract List<T> getTypes();
    public abstract T getType();


}
