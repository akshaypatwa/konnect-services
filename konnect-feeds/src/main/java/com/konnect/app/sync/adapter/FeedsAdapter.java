package com.konnect.app.sync.adapter;

import java.util.List;

public interface FeedsAdapter<T> {
	
	public List<T> syncFeeds(List<String> tags);
	public List<T> syncFeeds(List<String> tags,Long lastCrawl);
}
