package com.konnect.app.sync.adapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Thumbnail;
import com.konnect.app.pojo.Article;

@Component
public class YoutubeSearchAdapter implements FeedsAdapter<Article> {

    @Autowired
    Environment envProp;

    private static final String youTubeBase ="http://youtu.be/";
    @Override
    public List<Article> syncFeeds(List<String> tags) {
        return null;
    }

    @Override
    public List<Article> syncFeeds(List<String> tags, Long lastCrawl) {
        List<Article> articles = new ArrayList<>();
        try {
            // This object is used to make YouTube Data API requests. The last
            // argument is required, but since we don't need anything
            // initialized when the HttpRequest is initialized, we override
            // the interface and provide a no-op function.
            YouTube youtube = new YouTube.Builder(new NetHttpTransport(),  new JacksonFactory(), new HttpRequestInitializer() {
                @Override
                public void initialize(HttpRequest request) throws IOException {
                }
            }).setApplicationName("youtube-konnect-search").build();
            Long currentTime = System.currentTimeMillis();
            //Techcrunch and Google Talk channels
            String[] channels = new String[]{"UCCjyq_K1Xwfg8Lndy7lKMpA","UCtXKDgv1AVoG88PLl8nGXmw"};
            for (String tag : tags) {

                for (String channel : channels) {
                    // Define the API request for retrieving search results.
                    YouTube.Search.List search = youtube.search().list("id,snippet");


                    String apiKey = envProp.getProperty("konnect.feeds.youtube.api");
                    search.setKey(apiKey);
                    search.setQ(tag);

                    // Restrict the search results to only include videos.
                    search.setType("video");
                    search.setRegionCode("US");
                    search.setChannelId(channel);
                    // To increase efficiency, only retrieve the fields that the
                    // application uses.
                    search.setFields(
                                    "items(id/kind,id/videoId,snippet/title,snippet/thumbnails/high/url,snippet/thumbnails/default/url,snippet/publishedAt)");
                    Long limit = Long.valueOf(envProp.getProperty("konnect.feeds.youtube.limit"));
                    search.setMaxResults(limit);

                    // Call the API and print results.
                    SearchListResponse searchResponse = search.execute();
                    Iterator<SearchResult> iteratorSearchResults = searchResponse.getItems().iterator();
                    if (!iteratorSearchResults.hasNext()) {
                        System.out.println(" There aren't any results for your query.");
                    }

                    while (iteratorSearchResults.hasNext()) {

                        SearchResult singleVideo = iteratorSearchResults.next();
                        ResourceId rId = singleVideo.getId();

                        // Confirm that the result represents a video.
                        // Otherwise, the
                        // item will not contain a video ID.
                        if (rId.getKind().equals("youtube#video")) {

                            Thumbnail thumbnailDefault = singleVideo.getSnippet().getThumbnails().getHigh();
                            Thumbnail thumbnailStd = singleVideo.getSnippet().getThumbnails().getDefault();
                            Article article = new Article();
                            article.setTags(Collections.singletonList(tag));
                            article.setCategory("Videos");
                            article.setTitle(singleVideo.getSnippet().getTitle());
                            article.setLink(youTubeBase + singleVideo.getId().getVideoId());
                            article.setImageURL(thumbnailDefault.getUrl());
                            article.setImgStdResURL(thumbnailStd==null?"":thumbnailStd.getUrl());
                            article.setSource("YouTube");
                            article.setLastCrawlTime(currentTime);
                            article.setPublishedTime(singleVideo.getSnippet().getPublishedAt().getValue());
                            articles.add(article);
                            System.out.println(" Video Id" + rId.getVideoId());
                            System.out.println(" Title: " + singleVideo.getSnippet().getTitle());
                            System.out.println(" Thumbnail: " + thumbnailDefault.getUrl());
                            System.out.println(" Thumbnail: " + thumbnailStd.getUrl());
                            System.out.println("\n-------------------------------------------------------------\n");
                        }
                    }
                }

            }

        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                            + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return articles;
    }

}
