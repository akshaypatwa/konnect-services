package com.konnect.app.sync.adapter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Element;
import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Article;
import com.rometools.rome.feed.synd.SyndEntry;
@Component
public class BloombergRSSAdapter extends AbstractRSSAdapter<Article> {

    @Override
    public String getRSSUrl() {
        return "https://www.bloomberg.com/politics/feeds/site.xml";
    }

    @Override
    public String getCategory() {
        return "News";
    }

    @Override
    public String getSource() {
        return "Bloomberg";
    }

    @Override
    public void addThumbnail(SyndEntry entry, Article article) {
        for( Element fm : entry.getForeignMarkup()){
            String url = fm.getAttributeValue("url");

            if(StringUtils.isNotEmpty(url)){
                article.setImageURL(url);
                break;
            }
        }
        if(StringUtils.isEmpty(article.getImageURL())){
            article.setImageURL("");
        }
    }

    @Override
    public List<Article> getTypes() {
        return  new ArrayList<Article>();
    }

    @Override
    public Article getType() {
        return  new Article();
    }

}
