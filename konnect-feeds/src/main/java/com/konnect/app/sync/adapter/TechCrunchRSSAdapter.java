package com.konnect.app.sync.adapter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Element;
import org.springframework.stereotype.Component;

import com.konnect.app.pojo.Article;
import com.rometools.fetcher.FeedFetcher;
import com.rometools.fetcher.FetcherException;
import com.rometools.fetcher.impl.HttpURLFeedFetcher;
import com.rometools.rome.feed.synd.SyndCategory;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;

@SuppressWarnings("deprecation")
@Component
public class TechCrunchRSSAdapter extends AbstractRSSAdapter<Article> {

    @Override
    public List<Article> syncFeeds(List<String> tags) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public List<Article> syncFeeds(List<String> tags, Long lastCrawl) {
       return super.syncFeeds(tags, lastCrawl);
    }

	@Override
	public String getRSSUrl() {
		
		return "http://feeds.feedburner.com/TechCrunch/?fmt=xml";
	}

	@Override
	public String getCategory() {
		return "News";
	}

	@Override
	public String getSource() {
		return "TechCrunch";
	}

	@Override
	public void addThumbnail(SyndEntry entry, Article article) {
		for( Element fm : entry.getForeignMarkup()){
            if(StringUtils.equalsIgnoreCase(fm.getNamespace().getPrefix(),"media")){
                String imageUrl = fm.getAttribute("url").getValue();

                if (imageUrl.contains("?")) {
                    imageUrl = imageUrl.substring(0, imageUrl.indexOf('?'));
                }
                article.setImageURL(imageUrl);
                break;
            }
        }

	}

	@Override
	public List<Article> getTypes() {
		return  new ArrayList<Article>();
	}

	@Override
	public Article getType() {
		return  new Article();
	}
}
