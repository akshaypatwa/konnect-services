package com.konnect.app.tomcat.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.konnect.app.pojo.Goal;

@Path("goal")
public interface GoalService {

    @Path("all")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getGoals(@QueryParam("profileId") String profileId);
    
    @Path("allGoals")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getAllGoals(@QueryParam("profileId") String profileId);

    @Path("allMentee")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getGoalsOfMentees(@QueryParam("profileId") String profileId);

    @Path("create")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response create(Goal goal);

    @Path("{id}")
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    public Response delete(@PathParam("id") String goalId);

    @Path("readAll")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response readAll(@QueryParam("profileId") String profileId);
    
    @Path("feedback")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getGoalsWithFeedback(@QueryParam("profileId") String profileId, @QueryParam("isOwnProfile") String isOwnProfile);
}
