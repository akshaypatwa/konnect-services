package com.konnect.app.tomcat.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.konnect.app.pojo.LinkedInProfile;
import com.konnect.app.pojo.request.UpdateProfileDeviceToken;

/**
 * @author kazz
 *
 */
@Path("profile")
public interface ProfileService {

    @Path("{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getProfile(@PathParam("id") String id);

    @Path("")
    @PUT
    @Produces({ MediaType.APPLICATION_JSON })
    public Response updateProfile(LinkedInProfile profile);

    @Path("eliteProfile/{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getEliteProfile(@PathParam("id") String id);
    
    @Path("search/{search}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response search(@PathParam("search") String search,
                    @QueryParam("query") String query);
    
    @Path("searchProfiles")
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public Response SearchProfiles(@DefaultValue("20") @QueryParam("limit") int limit,
            @DefaultValue("0") @QueryParam("offset") int offset,
            @DefaultValue("false") @QueryParam("mentor") String mentor,
            @DefaultValue("false") @QueryParam("mentee") String mentee,
            @QueryParam("association") String association,
            @QueryParam("nearMe") String nearMe,
            @QueryParam("company") String company,
            @QueryParam("query") String name);
    
    @Path("{id}")
    @DELETE
    public Response delete(@PathParam("id") String id);

    @Path("deviceToken")
    @PUT
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response updateDeviceToken(UpdateProfileDeviceToken request);
    
    @Path("filter")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response filter(@QueryParam("mentor") boolean isMentor,
                    @QueryParam("mentee") boolean isMentee);

}
