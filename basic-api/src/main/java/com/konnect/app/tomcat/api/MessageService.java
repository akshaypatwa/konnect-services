package com.konnect.app.tomcat.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.konnect.app.pojo.Message;

@Path("message")
public interface MessageService {
    @Path("{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getMessage(@PathParam("id") String id);

    @Path("all")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response getAllMessages(@QueryParam("profileId") String profileId,
                    @DefaultValue("timestamp") @QueryParam("sort") String sort,
                    @DefaultValue("d") @QueryParam("sortOrder") String order);

    @Path("summary")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getMessagesSummary(@QueryParam("fromId") String fromId,@DefaultValue("10") @QueryParam("limit") Integer limit,
                    @DefaultValue("0") @QueryParam("offset") Integer offset,@DefaultValue("timestamp") @QueryParam("sort") String sort,
                    @DefaultValue("d") @QueryParam("sortOrder") String order);

    @Path("list")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getMessageSummaryList(@QueryParam("id") String profileId);

    @Path("unread")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getMessagesUnread(@QueryParam("fromId") String fromId,@DefaultValue("10") @QueryParam("limit") int limit,
                    @DefaultValue("0") @QueryParam("offset") int offset,@DefaultValue("timestamp") @QueryParam("sort") String sort,
                    @DefaultValue("d") @QueryParam("sortOrder") String order);

    @Path("")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getMessageList(@DefaultValue("10") @QueryParam("limit") int limit,
                    @DefaultValue("0") @QueryParam("offset") int offset,
                    @QueryParam("fromId") String fromId,
                    @QueryParam("toId") String toId,
                    @DefaultValue("timestamp") @QueryParam("sort") String sort,
                    @DefaultValue("d") @QueryParam("sortOrder") String order);

    @Path("create")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response create(Message msg);

    @Path("{id}")
    @PUT
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response update(Message msg);

    @Path("{id}")
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    public Response delete(@PathParam("id") String id);

    @Path("readall")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response readAll(
                    @QueryParam("fromId") String fromId,
                    @QueryParam("toId") String toId);

}
