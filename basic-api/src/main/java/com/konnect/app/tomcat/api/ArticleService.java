package com.konnect.app.tomcat.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.konnect.app.pojo.Article;

@Path("article")
public interface ArticleService {
	@Path("{id}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getArticle(@PathParam("id") String id);
	
	@Path("")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getArticles(@DefaultValue("10") @QueryParam("limit") int limit,
			@DefaultValue("0") @QueryParam("offset") int offset,
			@QueryParam("tags") List<String> tags,
			@QueryParam("category") String category,
			@QueryParam("profileId") String profileId);
	
	@Path("create")
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response create(Article tag);
	
	@Path("{id}")
	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response update(Article tag);
	
	@Path("{id}/{profileId}")
	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	public Response delete(@PathParam("id") String id, @PathParam("profileId") String profilId);
	
	@Path("sync")
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response syncArticles();
}
