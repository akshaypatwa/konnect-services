package com.konnect.app.tomcat.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.konnect.app.pojo.Network;

/**
 * Mentor/mentee network requests.
 *
 * @author kazz
 *
 */
@Path("network")
public interface NetworkService {

    @Path("mentor/{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response getMentors(@PathParam("id") String profileId);

    @Path("mentee/{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response getMentee(@PathParam("id") String profileId);

    @Path("pending/{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response getPending(@PathParam("id") String profileId);

    @Path("request")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response postRequest(Network network);
    
    @Path("requestRevised")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response postRequest(@QueryParam("profile1") String profile1,
    							@QueryParam("profile2") String profile2,
    							@QueryParam("status") String status);

    @Path("{id}")
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    public Response delete(@PathParam("id") String id);

    @Path("updateimageurl")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response updateImageUrl(@QueryParam("profileId") String profilId, 
    								  @QueryParam("imageUrl") String imageUrl);
    
    @Path("{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response getNetworkList(@PathParam("id") String profileId);


    @Path("updateall")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response updateall(@QueryParam("profileId") String profilId,
                    @QueryParam("imageUrl") String imageUrl);
    
    @Path("suggestions/{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response getSuggestions(@PathParam("id") String profileId);
}
