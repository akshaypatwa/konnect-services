package com.konnect.app.tomcat.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author kazz
 *
 */
@Path("login")
public interface LoginService {

    @Path("link")
    @GET
    @Produces( { MediaType.TEXT_PLAIN } )
    public Response linkAccount(@QueryParam("code") String code,
                    @Context UriInfo ui);


    @Path("success")
    @GET
    @Produces( {MediaType.TEXT_PLAIN })
    public Response authCompleted(@QueryParam("id") String id);

    @Path("status")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response statusChange(@QueryParam("profileId") String profileId, @QueryParam("status") String status);


}
