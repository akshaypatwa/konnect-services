package com.konnect.app.tomcat.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("image")
public interface ImageService {

    //    public static final String FILE_PATH = "/tmp/";

    public static int MAX_WIDTH = 500;

    @GET
    @Path("")
    @Produces("image/png")
    public Response getImage(@Context UriInfo uriInfo, @QueryParam("fileName") String fileName, @QueryParam("w") int width);

}
