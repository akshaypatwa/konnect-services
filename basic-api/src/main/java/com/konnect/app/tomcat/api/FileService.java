package com.konnect.app.tomcat.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;

@Path("file")
public interface FileService {

    @Path("upload")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes("multipart/form-data")
    public Response upload(@Multipart("file") Attachment obj);

    @Path("uploadArtifact")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes("multipart/form-data")
    public Response uploadArtifact(@Multipart("file") Attachment obj);


    @Path("timestamp")
    @GET
    @Produces({ MediaType.TEXT_PLAIN })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response getTimestamp(@QueryParam("filename") String filename);


}
