package com.konnect.app.tomcat.api;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author kazz
 *
 */
@Path("admin")
public interface AdminService {

    @Path("migrateS3")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response migrateS3(@HeaderParam("Authorization") String authString);

    @Path("apns")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    Response sendPushNotification(@QueryParam("destination") String destination, @HeaderParam("Authorization") String authString);

}
