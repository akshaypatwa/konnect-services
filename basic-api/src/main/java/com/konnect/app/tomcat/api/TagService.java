package com.konnect.app.tomcat.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.konnect.app.pojo.Tag;

/**
 * @author kazz
 *
 */
@Path("tag")
public interface TagService {

    @Path("{id}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getTag(@PathParam("id") String id);

    @Path("")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getTags(@DefaultValue("10") @QueryParam("limit") int limit,
                    @DefaultValue("0") @QueryParam("offset") int offset);

    @Path("/search/{s}")
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response search(@PathParam("s") String s);

    @Path("create")
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response create(Tag tag);

    @Path("{id}")
    @PUT
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response update(Tag tag);

    @Path("{id}")
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    public Response delete(@PathParam("id") String id);


}
