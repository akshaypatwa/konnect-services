package com.konnect.app.sync;

import java.util.List;

import com.konnect.app.pojo.Article;

public interface FeedSyncFacade {
	
	public void syncFeeds();
	public List<Article> getFeeds(List<String> tags);
}
